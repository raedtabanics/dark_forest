/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID HUNGMAN_TREE = 2664698634U;
        static const AkUniqueID MUSICPLAY_CREATURESIGHTING = 1544334737U;
        static const AkUniqueID MUSICPLAY_CREATURESTALKSTART = 219782959U;
        static const AkUniqueID MUSICPLAY_MAINTHEME = 2679047715U;
        static const AkUniqueID MUSICPLAY_NOON = 249957453U;
        static const AkUniqueID MUSICPLAY_SUNSET = 668937225U;
        static const AkUniqueID MUSICPLAY_WAKEUP = 2137688908U;
        static const AkUniqueID PLAY_ADR_DEATH = 1788320722U;
        static const AkUniqueID PLAY_ADR_RUN = 4289476203U;
        static const AkUniqueID PLAY_ADR_SHIVER = 3258352129U;
        static const AkUniqueID PLAY_AMB = 2959533290U;
        static const AkUniqueID PLAY_ARROW_DRAW = 2761893146U;
        static const AkUniqueID PLAY_ARROW_IMPACT = 2483542346U;
        static const AkUniqueID PLAY_ARROW_SHOOTING = 2226322255U;
        static const AkUniqueID PLAY_ARROW_STING = 2186586703U;
        static const AkUniqueID PLAY_ARROW_WOOSH = 2419754028U;
        static const AkUniqueID PLAY_BEAR = 1996054844U;
        static const AkUniqueID PLAY_BOAR = 1391074922U;
        static const AkUniqueID PLAY_BUTTONMENU = 805491187U;
        static const AkUniqueID PLAY_CALCFOOTSTEP = 1333092035U;
        static const AkUniqueID PLAY_CRAFT = 622370216U;
        static const AkUniqueID PLAY_CROWS = 639442158U;
        static const AkUniqueID PLAY_DRAWARROW = 1897747491U;
        static const AkUniqueID PLAY_DROPITEM = 2933471780U;
        static const AkUniqueID PLAY_ETHEREAL_DEEPFOREST = 1550413066U;
        static const AkUniqueID PLAY_FIRE = 3015324718U;
        static const AkUniqueID PLAY_FLASHLIGHT = 2149098184U;
        static const AkUniqueID PLAY_FOOTSTEP = 1602358412U;
        static const AkUniqueID PLAY_GRAB_ITEM = 130877718U;
        static const AkUniqueID PLAY_GRABJURNAL = 1972053644U;
        static const AkUniqueID PLAY_HOPBUNNY = 2329264733U;
        static const AkUniqueID PLAY_ITEMGRAB = 2521947797U;
        static const AkUniqueID PLAY_KNIFE = 2169194149U;
        static const AkUniqueID PLAY_KNIFEIMPACT = 1088170375U;
        static const AkUniqueID PLAY_KNIFETHROW = 205718267U;
        static const AkUniqueID PLAY_LOAD = 4098463250U;
        static const AkUniqueID PLAY_MASKON = 2823752901U;
        static const AkUniqueID PLAY_OPENINVENTORY = 1432112630U;
        static const AkUniqueID PLAY_OWL2 = 1861994186U;
        static const AkUniqueID PLAY_OWLS = 1861994123U;
        static const AkUniqueID PLAY_POPUP_MENU = 1944359270U;
        static const AkUniqueID PLAY_READINGJOURNAL = 818804071U;
        static const AkUniqueID PLAY_RIVER = 1498169336U;
        static const AkUniqueID PLAY_SELECTITEM = 2240639679U;
        static const AkUniqueID PLAY_STARTINGHUNT = 361766177U;
        static const AkUniqueID PLAY_TIGHTNESS = 2910261091U;
        static const AkUniqueID PLAY_TORCH = 2025845440U;
        static const AkUniqueID PLAY_TORCHLIT = 541501859U;
        static const AkUniqueID PLAY_TOURCHOUT = 1492574735U;
        static const AkUniqueID PLAY_TREE = 3492314494U;
        static const AkUniqueID START_ROOMTONE = 3454424619U;
        static const AkUniqueID STOP_MUSIC = 2837384057U;
        static const AkUniqueID STOP_TORCH_FIRE = 2110264497U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace FORESTLOCATION
        {
            static const AkUniqueID GROUP = 2018684723U;

            namespace STATE
            {
                static const AkUniqueID BOSS = 1560169506U;
                static const AkUniqueID LEVEL1 = 2678230382U;
                static const AkUniqueID LEVEL2 = 2678230381U;
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID SAFEAREA = 526233797U;
            } // namespace STATE
        } // namespace FORESTLOCATION

        namespace TIMEOFDAY
        {
            static const AkUniqueID GROUP = 3729505769U;

            namespace STATE
            {
                static const AkUniqueID DAWN = 2009803003U;
                static const AkUniqueID DUSK = 2348606646U;
                static const AkUniqueID MIDNIGHT = 3491177121U;
                static const AkUniqueID MORNING = 1924633667U;
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID NOON = 765672767U;
                static const AkUniqueID SUNSET = 2332133503U;
            } // namespace STATE
        } // namespace TIMEOFDAY

    } // namespace STATES

    namespace SWITCHES
    {
        namespace AMB
        {
            static const AkUniqueID GROUP = 1117531639U;

            namespace SWITCH
            {
                static const AkUniqueID AMB_COZY = 3823486911U;
                static const AkUniqueID AMB_DAY = 3453085348U;
                static const AkUniqueID AMB_DEEPFOREST = 2367986463U;
                static const AkUniqueID AMB_OPENSPACE = 3698351640U;
            } // namespace SWITCH
        } // namespace AMB

        namespace ARROWHIT
        {
            static const AkUniqueID GROUP = 2212241627U;

            namespace SWITCH
            {
                static const AkUniqueID BODY = 1845389165U;
                static const AkUniqueID DIRT = 2195636714U;
                static const AkUniqueID WATER = 2654748154U;
                static const AkUniqueID WOOD = 2058049674U;
            } // namespace SWITCH
        } // namespace ARROWHIT

        namespace FOOTSTEP_MATERIAL
        {
            static const AkUniqueID GROUP = 684570577U;

            namespace SWITCH
            {
                static const AkUniqueID GRASS = 4248645337U;
                static const AkUniqueID RIVERSHORE = 4128758080U;
                static const AkUniqueID TALLGRASS = 659148630U;
                static const AkUniqueID WOOD = 2058049674U;
            } // namespace SWITCH
        } // namespace FOOTSTEP_MATERIAL

        namespace FOOTSTEP_TYPE
        {
            static const AkUniqueID GROUP = 2615620554U;

            namespace SWITCH
            {
                static const AkUniqueID RUN = 712161704U;
                static const AkUniqueID WALK = 2108779966U;
            } // namespace SWITCH
        } // namespace FOOTSTEP_TYPE

        namespace KNIFEIMPACT
        {
            static const AkUniqueID GROUP = 572213626U;

            namespace SWITCH
            {
                static const AkUniqueID DIRT = 2195636714U;
                static const AkUniqueID FLESH = 1153642577U;
                static const AkUniqueID WOOD = 2058049674U;
            } // namespace SWITCH
        } // namespace KNIFEIMPACT

        namespace RUN_FOOTSTEPS
        {
            static const AkUniqueID GROUP = 1870592208U;

            namespace SWITCH
            {
                static const AkUniqueID GRASS = 4248645337U;
                static const AkUniqueID RIVERSHORE = 4128758080U;
                static const AkUniqueID TALLGRASS = 659148630U;
                static const AkUniqueID WOOD = 2058049674U;
            } // namespace SWITCH
        } // namespace RUN_FOOTSTEPS

        namespace TIGHTNESS
        {
            static const AkUniqueID GROUP = 3431563186U;

            namespace SWITCH
            {
                static const AkUniqueID FAST = 2965380179U;
                static const AkUniqueID LONG = 674228435U;
                static const AkUniqueID MEDIUM = 2849147824U;
            } // namespace SWITCH
        } // namespace TIGHTNESS

        namespace WALKING_RUNNING
        {
            static const AkUniqueID GROUP = 3407546846U;

            namespace SWITCH
            {
                static const AkUniqueID RUNNINGGRASS = 4188896612U;
                static const AkUniqueID RUNNINGMUD = 3336443568U;
                static const AkUniqueID RUNNINGWOOD = 1746733949U;
                static const AkUniqueID WALKINGGRASS = 4075501324U;
                static const AkUniqueID WALKINGMUD = 1530114120U;
                static const AkUniqueID WALKINGWOOD = 1660741333U;
            } // namespace SWITCH
        } // namespace WALKING_RUNNING

    } // namespace SWITCHES

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MAINSOUNDBANK = 534561221U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
