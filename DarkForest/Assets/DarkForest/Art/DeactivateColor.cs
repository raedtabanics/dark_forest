using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DeactivateColor : MonoBehaviour
{

    public Text theText;
    public Color disableColor;
    public Color enabledColor;

    public Button yourButton;

    void Start()
    {
        Button btn = yourButton.GetComponent<Button>();
    }
    void Update()
    {
        if (yourButton.interactable == false)
        {
            theText.color = disableColor;
        }
        else
        {
            theText.color = enabledColor;
        }
    
    }
}