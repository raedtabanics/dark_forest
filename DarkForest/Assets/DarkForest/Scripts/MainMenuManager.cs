using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuManager : MonoBehaviour
{

    [SerializeField] private GameObject CreditsPanel;

    private bool isCreditPanelActive = false;


    public void ShowCredits()
    {
        CreditsPanel.SetActive(true);
    }
    
    public void ExitGame()
    {
        Application.Quit();
    }


    private void Awake()
    {
        CreditsPanel.SetActive(false);
    }


    private void Update()
    {
        if (CreditsPanel.activeSelf == false)
        {
            return;
        }

        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Backspace) || Input.GetKeyDown (KeyCode.Mouse0))
        {
            CreditsPanel.SetActive (false);
        }
    }
}
