using UnityEngine;

public enum TimeOfDayState
{
    Dawn, Morning, Noon, Dusk, Sunset, Midnight
}