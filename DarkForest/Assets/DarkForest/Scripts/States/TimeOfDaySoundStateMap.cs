using Sirenix.OdinInspector;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName = "TimeOfDaySoundStateMap", menuName = "DarkForest/TimeOfDaySoundStateMap", order = 0)]
public class TimeOfDaySoundStateMap : SerializedScriptableObject 
{
    [SerializeField]
    private Dictionary<TimeOfDayState, AK.Wwise.State> TimeOfDaySoundStates;


    public AK.Wwise.State GetState (TimeOfDayState state) { return TimeOfDaySoundStates[state]; }
}