using UnityEngine;

public enum ForestDepthState
{
    Safe, Level1, Level2, Boss
}