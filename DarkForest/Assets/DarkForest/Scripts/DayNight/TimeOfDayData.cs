using System;
using Sirenix.OdinInspector;
using DapperDino.DapperTools.ScriptableEvents.Events;
using UnityEngine;

[CreateAssetMenu (fileName = "TimeOfDayData", menuName = "TimeOfDayData")]
public class TimeOfDayData : ScriptableObject 
{

    ///////////////////////////////////////////////////////////
    // SUN PROPERTIES
    ///////////////////////////////////////////////////////////

    [Space (5), FoldoutGroup ("Sun"), Tooltip ("The multiplier used to transition to the next time of day. Keep both the Time value (x), and Value, err, value (y), between 0 and 1. Keep linear for day time, but for sunsets, night-time, and dawn, it can be useful to try exponential or logarithmic curves to ease in and out of different settings.")]
    public AnimationCurve FadeToNextTimeCurve = new AnimationCurve (new Keyframe (0, 0), new Keyframe (1, 1));
    

    [Header ("Time"), Range (0, 23), FoldoutGroup ("Sun")]
    public int Hour;


    [Range (0, 59), FoldoutGroup ("Sun")]
    public int Minute;


    [Header ("Intensity"), Range (0, 1), FoldoutGroup ("Sun")]
    public float SunIntensity;

    
    [Range (0, 1), FoldoutGroup ("Sun"), Tooltip ("Controls the brightness of the skybox lighting in the Scene.")]
    public float SkyboxIntensityMultiplier;


    [Range (0, 1), FoldoutGroup ("Sun"), Tooltip ("Interpolates between the Dim intensity (0) and Normal intensity (1)")]
    public float SkylightIntensityPercentage;



    [Header ("Color"), FoldoutGroup ("Sun")]
    public bool ShouldSkyboxColorMirrorSunColor = true;


    [FoldoutGroup ("Sun")]
    public Color SunColor;


    [FoldoutGroup ("Sun"), HideIf ("ShouldSkyboxColorMirrorSunColor")]
    public Color SkyboxTint;


    [Header ("Angle"), FoldoutGroup ("Sun")]
    public Vector3 SunEulerAngle;


    [FoldoutGroup ("Sun"), Range (0, 360)]
    public float SkyboxRotation;



    public Color GetSkyboxColor()
    {
        if (ShouldSkyboxColorMirrorSunColor)
        {
            return SunColor;
        }

        return SkyboxTint;
    }


    
    ///////////////////////////////////////////////////////////
    // SKYBOX PROPERTIES
    ///////////////////////////////////////////////////////////
    



    // [FoldoutGroup ("Skybox"), Tooltip ("Ambient lighting coming from above.")]
    // public Color SkyColor;


    // [FoldoutGroup ("Skybox"), Tooltip ("Ambient lighting coming from the sides.")]
    // public Color EquatorColor;
    

    // [FoldoutGroup ("Skybox"), Tooltip ("Ambient lighting coming from below.")]
    // public Color GroundColor;


    // [FoldoutGroup ("Skybox"), Tooltip ("Flat ambient lighting color.")]
    // public Color AmbientLightColor;
    



    
    ///////////////////////////////////////////////////////////
    // METADATA & EVENTS
    ///////////////////////////////////////////////////////////


    [FoldoutGroup ("Events")]
    public VoidEvent TimeChangeEvent;


    [FoldoutGroup ("Events")]
    public AK.Wwise.State WwiseState;


    [FoldoutGroup("Events")] 
    public AK.Wwise.Event WwiseEvent;


    
    ///////////////////////////////////////////////////////////
    // METHODS
    ///////////////////////////////////////////////////////////
    
    
    public TimeSpan GetTime()
    {
        return TimeSpan.FromHours (Hour) + TimeSpan.FromMinutes (Minute);
    }


    public DateTime GetDateTime()
    {
        return DateTime.Now + GetTime();
    }


}