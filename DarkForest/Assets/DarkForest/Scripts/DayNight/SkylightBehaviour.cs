using Sirenix.OdinInspector;
using UnityEngine;

public class SkylightBehaviour : MonoBehaviour
{
    [SerializeField]
    private bool ShouldUseLightIntensityValue = true;


    [SerializeField, HideIf ("ShouldUseLightIntensityValue")]
    private float NormalIntensity;
    

    [SerializeField, HideIf ("ShouldUseLightIntensityValue")]
    private float DimIntensity = 0.0f;


    private Light light;


    private void Awake()
    {
        light = GetComponent<Light>();

        if (ShouldUseLightIntensityValue)
        {
            NormalIntensity = light.intensity;
            DimIntensity    = 0.0f;
        }
    }

    public void AdjustIntensity (float normalizedValue)
    {
        if (! Application.isPlaying) { light = GetComponent<Light>(); }

        light.intensity = NormalIntensity * normalizedValue;
    }
}