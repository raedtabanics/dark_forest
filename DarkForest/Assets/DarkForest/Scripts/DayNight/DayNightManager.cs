using System.Collections.Generic;
using System.Collections;
using System;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;

public class DayNightManager : Singleton<DayNightManager>
{


    ///////////////////////////////////////////////////////////
    // PROPERTIES
    ///////////////////////////////////////////////////////////


    public event Action OnSwitchTimeOfDay;


    [SerializeField, InlineEditor (InlineEditorObjectFieldModes.Foldout), OnValueChanged ("UpdateLightInEditor", includeChildren: true), OnCollectionChanged ("UpdateLightInEditor")]
    private List<TimeOfDayData> significantTimesOfDay = new List<TimeOfDayData>(); 


    [SerializeField]
    private StringEvent uiUpdateEvent;


    [SerializeField]
    private FloatEvent timeUpdateEvent;


    [SerializeField]
    private Light directionalLight = null;


    [SerializeField]
    private float timeMultiplier = 1.0f;
    

    [SerializeField]
    private TimeOfDayData startingSignificantTimeOfDay;



    [SerializeField, OnValueChanged ("UpdateLightInEditor"), Range (0.0f, 23.99f), Tooltip ("Okay so to make this just one float value, its a bit funky. To the left of the decimal place is the number of hours (0 - 23) as you would expect, but the decimal is the percentage through that hour; so 12.50 means it's 12:30. 12.75 = 12:45, 12.25 = 12:15.")]
    private float CurrentTimeInEditor;


    [SerializeField]
    private bool ShouldStartGameWithEditorTime = false;



    private void UpdateLightInEditor()
    {

        if (Application.isPlaying) { return; }

        if (skylights == null)
        {
            skylights = GameObject.FindObjectsOfType<SkylightBehaviour> (includeInactive: true)
                                  .ToList();
        }

        SwitchToClosestSignificantTime (DateTime.MinValue);
    }


    private void SwitchToClosestSignificantTime (DateTime _currentTime)
    {
        if (_currentTime == DateTime.MinValue)
        {
            CurrentTime = DateTime.Now.Date + TimeSpan.FromHours (Math.Floor (CurrentTimeInEditor)) + TimeSpan.FromMinutes ((CurrentTimeInEditor % 1.0f) * 60); 
        }


        for (int i = significantTimesOfDay.Count - 1; i >= 0; i--)
        {
            var time = significantTimesOfDay[i];

            if ((CurrentTime.Hour * 60) + CurrentTime.Minute >= (time.Hour * 60) + time.Minute)
            {
                Debug.Log (string.Format ("Editor is at significant time: {0}, time is: {1}", time.name, CurrentTime.ToLongTimeString()));
                CurrentSignificantTime = time;
                UpdateTimeOfDay();
                return;
            }
        }

        Debug.Log ("Hmm, something is wrong");
    }

    private DateTime CurrentTime {get; set;}


    private List<SkylightBehaviour> skylights;
    
    


    ///////////////////////////////////////////////////////////
    // UNITY METHODS
    ///////////////////////////////////////////////////////////
    
    
    private void Awake()
    {
        Instance = this;
    }


    private void OnEnable()
    {
        skylights = GameObject.FindObjectsOfType<SkylightBehaviour> (includeInactive: true)
                              .ToList();
    }


    private void Start()
    {
        if (ShouldStartGameWithEditorTime)
        {
            SwitchToClosestSignificantTime (DateTime.MinValue);
        }

        else
        {
            if (startingSignificantTimeOfDay == null)
            {
                startingSignificantTimeOfDay = significantTimesOfDay[0];
            }

            CurrentTime = DateTime.Now.Date + startingSignificantTimeOfDay.GetTime();
            CurrentSignificantTime = startingSignificantTimeOfDay;
        }



        if (directionalLight == null)
        {
            Debug.Log ("Please give reference to the directional light in the Day/Night manager!");
        }


        UpdateTimeOfDay();
        StartCoroutine (TickTimeRoutine());
    }


    private void OnDisable()
    {
        StopAllCoroutines();
    }


    
    
    ///////////////////////////////////////////////////////////
    // OUR UPDATE
    ///////////////////////////////////////////////////////////


    private WaitForSeconds timeToWait = new WaitForSeconds (0.1f);

    private IEnumerator TickTimeRoutine()
    {
        while (true)
        {
            float dt = Time.deltaTime * timeMultiplier;

            CurrentTime = CurrentTime.AddSeconds (dt);
            UpdateTimeOfDay();

            if (uiUpdateEvent != null)
            {
                uiUpdateEvent.Raise (CurrentTime.ToString ("HH:mm:ss"));
            }

            if (timeUpdateEvent != null)
            {
                timeUpdateEvent.Raise (-dt);
            }

            yield return null;
        }
    }


    
    
    
    ///////////////////////////////////////////////////////////
    // TIME OF DAY LOGIC
    ///////////////////////////////////////////////////////////


    private TimeOfDayData currentSignificantTime; 


    public TimeOfDayData CurrentSignificantTime
    {
        get => currentSignificantTime;

        private set
        {
            currentSignificantTime = value;

            OnSwitchTimeOfDay?.Invoke();

            if (currentSignificantTime.TimeChangeEvent != null && Application.isPlaying)  // if this is being called from out of play mode, don't raise the event.
            {
                currentSignificantTime.TimeChangeEvent.Raise();
            }

            secondsBetweenCurrentAndNextSignificantTimes = Extensions.GetRolloverDifference (NextSignificantTime.GetTime(), currentSignificantTime.GetTime())
                                                                     .TotalSeconds;
        }
    }

    
    private TimeOfDayData NextSignificantTime
    {
        get
        {
            if (currentSignificantTime == significantTimesOfDay [significantTimesOfDay.Count - 1])
            {
                return significantTimesOfDay [0];
            }

            return significantTimesOfDay [significantTimesOfDay.IndexOf (currentSignificantTime) + 1];
        }
    }


    private double secondsBetweenCurrentAndNextSignificantTimes;


    private void UpdateTimeOfDay()
    {
        var next = NextSignificantTime;

        //Debug.Log (CurrentTime.Hour);

        // the world's laziest circular array implementation lmao
        if (CurrentTime.Hour == next.Hour && CurrentTime.Minute == next.Minute)
        {
            CurrentSignificantTime = next;
        }




        ///////////////////
        // 0.0 means you just changed significant times, 1.0 means you're about to change significant times.

        float normalizedTimeToNextSignificantTime = 1.0f - (float)(Extensions.GetRolloverDifference (NextSignificantTime.GetTime(), CurrentTime.TimeOfDay)
                                                                             .TotalSeconds 
                                                                             / secondsBetweenCurrentAndNextSignificantTimes);


        // now the normalized value has been weighted with the animation curve
        normalizedTimeToNextSignificantTime = CurrentSignificantTime.FadeToNextTimeCurve.Evaluate (normalizedTimeToNextSignificantTime);



        ///////////////
        // LIGHT

        directionalLight.color              = Color.Lerp (CurrentSignificantTime.SunColor, NextSignificantTime.SunColor, normalizedTimeToNextSignificantTime);
        directionalLight.intensity          = Freya.Mathfs.LerpClamped (currentSignificantTime.SunIntensity, NextSignificantTime.SunIntensity, normalizedTimeToNextSignificantTime);
        directionalLight.transform.rotation = Quaternion.Slerp (Extensions.QuatFromEuler (CurrentSignificantTime.SunEulerAngle), 
                                                                Extensions.QuatFromEuler (NextSignificantTime.SunEulerAngle), 
                                                                normalizedTimeToNextSignificantTime);




        ////////////////
        // SKYBOX

        RenderSettings.ambientIntensity = Freya.Mathfs.LerpClamped (currentSignificantTime.SkyboxIntensityMultiplier, NextSignificantTime.SkyboxIntensityMultiplier, normalizedTimeToNextSignificantTime);


        // RenderSettings.ambientSkyColor      = Color.Lerp (CurrentSignificantTime.SkyColor,          NextSignificantTime.SkyColor,          normalizedTimeToNextSignificantTime);
        // RenderSettings.ambientEquatorColor  = Color.Lerp (CurrentSignificantTime.EquatorColor,      NextSignificantTime.EquatorColor,      normalizedTimeToNextSignificantTime);
        // RenderSettings.ambientGroundColor   = Color.Lerp (CurrentSignificantTime.GroundColor,       NextSignificantTime.GroundColor,       normalizedTimeToNextSignificantTime);
        // RenderSettings.ambientLight         = Color.Lerp (CurrentSignificantTime.AmbientLightColor, NextSignificantTime.AmbientLightColor, normalizedTimeToNextSignificantTime);


        RenderSettings.skybox.SetColor ("_Tint", Color.Lerp (CurrentSignificantTime.GetSkyboxColor(), NextSignificantTime.GetSkyboxColor(), normalizedTimeToNextSignificantTime));
        RenderSettings.skybox.SetFloat ("_Rotation", Freya.Mathfs.LerpClamped (CurrentSignificantTime.SkyboxRotation, NextSignificantTime.SkyboxRotation, normalizedTimeToNextSignificantTime));

        foreach (var skylight in skylights)
        {
            skylight.AdjustIntensity (Freya.Mathfs.LerpClamped (currentSignificantTime.SkylightIntensityPercentage, NextSignificantTime.SkylightIntensityPercentage, normalizedTimeToNextSignificantTime));
        }

    }
}