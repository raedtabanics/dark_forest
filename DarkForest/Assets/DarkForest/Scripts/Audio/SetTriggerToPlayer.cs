using System.Collections.Generic;
using UnityEngine;


public class SetTriggerToPlayer : MonoBehaviour
{


    [SerializeField]
    private List<AkTriggerEnter> EnterTriggers = new List<AkTriggerEnter>();


    [SerializeField]
    private List<AkTriggerExit> ExitTriggers = new List<AkTriggerExit>();


    private void Awake()
    {

        GameObject playerObj = GameObject.FindGameObjectWithTag ("Player");

        if (playerObj == null) 
        { 
            Debug.Log ("Somebody, somewhere, fucked up, son. This audio trigger can't find the damn player!");
            return;
        }

        foreach (var trigger in EnterTriggers)
        {
            trigger.triggerObject = playerObj;
        }


        foreach (var trigger in ExitTriggers)
        {
            trigger.triggerObject = playerObj;
        }
    }
}