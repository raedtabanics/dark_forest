using UnityEngine;
using Sirenix.OdinInspector;
using System.Collections.Generic;

public class AudioManager : Singleton<AudioManager>
{


    [SerializeField] 
    private GameObject piano;



    [SerializeField, InlineEditor (InlineEditorObjectFieldModes.Foldout)]
    private TimeOfDaySoundStateMap timeOfDaySoundStateMap;



    public void SwitchAmbienceTrack()
    {
        if (DayNightManager.Instance.CurrentSignificantTime.WwiseState == null) { return; }
        
        DayNightManager.Instance.CurrentSignificantTime.WwiseState.SetValue();

        if (DayNightManager.Instance.CurrentSignificantTime.WwiseEvent != null && piano != null)
        {
            DayNightManager.Instance.CurrentSignificantTime.WwiseEvent.Post (piano);
        }

        //Debug.Log ("hi asdasds");
        Debug.Log (string.Format ("Switching to {0} ambience", DayNightManager.Instance.CurrentSignificantTime.WwiseState.Name));
    }



    private void Awake()
    {
        Instance = this;
    }

    private void Start()  
    { 
        DayNightManager.Instance.OnSwitchTimeOfDay += SwitchAmbienceTrack; 
    }

    private void OnDisable() 
    { 
        DayNightManager.Instance.OnSwitchTimeOfDay -= SwitchAmbienceTrack; 
    }

}