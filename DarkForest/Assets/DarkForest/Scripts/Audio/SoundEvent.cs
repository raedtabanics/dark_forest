using UnityEngine;

public class SoundEvent : MonoBehaviour
{
    public AK.Wwise.Event soundEvent;


    public void Play()
    {
        soundEvent.Post(this.gameObject);
    }
}