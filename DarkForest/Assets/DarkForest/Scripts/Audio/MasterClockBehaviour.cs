using System;
using UnityEngine;


[RequireComponent (typeof (AudioSource))]
public class MasterClockBehaviour : MonoBehaviour
{
    public static double DeltaTime       { get; private set; }
    public static double SampleTimer     { get; private set; }
    public static double BufferInSeconds { get; private set; }

    public static Action<double> OnTickFuncs;

    private void Awake()
    {
        SampleTimer = AudioSettings.dspTime;
        
        int bufferLength, dummy;
        AudioSettings.GetDSPBufferSize (out bufferLength, out dummy);
        BufferInSeconds = (double)bufferLength / AudioSettings.outputSampleRate; 
    }

    private void OnAudioFilterRead (float[] data, int channels)
    {
        DeltaTime    = AudioSettings.dspTime - SampleTimer;
        SampleTimer += DeltaTime;
        OnTickFuncs?.Invoke (DeltaTime);
    }
}