using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.AddressableAssets;
using System.Threading.Tasks;


public static class Prefabs
{
    public static List<GameObject> _assets = new List<GameObject>();

    //- prefabs
    public static void AddAsset(GameObject obj){

        _assets.Add(obj);
    }
    public static GameObject GetAsset(string name){
        return _assets.FirstOrDefault((x)=>x.name.ToLower().Equals(name.ToLower()));
        
    }

    //- Addressables
    public static Task<GameObject> GetAddressable(string assetId){
        var tcs = new TaskCompletionSource<GameObject>();
        var asset = Addressables.LoadAssetAsync<GameObject>(assetId);
        asset.Completed += operation => { tcs.SetResult(operation.Result); };
        return tcs.Task;
    }

    public static Task<T> GetAddressable<T>(string address)
    {
        var tcs = new TaskCompletionSource<T>();
        var asset = Addressables.LoadAssetAsync<T>(address);
        asset.Completed += operation => { tcs.SetResult(operation.Result); };
        return tcs.Task;
    }

   
    
}
