﻿using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class StableCollection<Type, CollectionType> : ICollection<Type>, IEnumerable<Type>, IReadOnlyCollection<Type> where CollectionType : ICollection<Type> 
{
    private CollectionType m_UnstableCollection, m_StableCollection;


    public int Count => throw new NotImplementedException();

    public bool IsReadOnly => throw new NotImplementedException();

    public void Add(Type item)
    {
        throw new NotImplementedException();
    }

    public void Clear()
    {
        throw new NotImplementedException();
    }

    public bool Contains(Type item)
    {
        throw new NotImplementedException();
    }

    public void CopyTo(Type[] array, int arrayIndex)
    {
        throw new NotImplementedException();
    }

    public IEnumerator<Type> GetEnumerator()
    {
        throw new NotImplementedException();
    }

    public bool Remove(Type item)
    {
        throw new NotImplementedException();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        throw new NotImplementedException();
    }
}

