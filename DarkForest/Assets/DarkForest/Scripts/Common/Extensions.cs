using UnityEngine;
using System;

public static class Extensions
{
    public static TimeSpan GetRolloverDifference (TimeSpan toTime, TimeSpan fromTime)
    {
        TimeSpan dt = toTime - fromTime;

        if (dt.TotalSeconds < 0.0) 
        {
            dt += TimeSpan.FromHours (24.0);
        }

        return dt;
    }


    public static Quaternion QuatFromEuler (Vector3 euler)
    {
        return Quaternion.Euler (euler.x, euler.y, euler.z);
    }
}