using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;
public class ItemData 
{
    public string itemId {get ; private set;}
    public string itemName {get ; private set;}

    public ItemType itemType{get; private set;}

    public Sprite sprite;
    public GameObject itemReference;
    public ItemData(string itemId, string itemName,ItemType itemType,GameObject itemReference){
        this.itemId = itemId;
        this.itemName = itemName;
        this.itemType = itemType;
        this.itemReference = itemReference;
        Setup();
    }

    public async Task<bool> Setup(){
        sprite = await Prefabs.GetAddressable<Sprite>(itemId);
        Debug.Log(sprite.name);
        return true;
    }
}


public enum ItemType{
    WEAPON,
    FOOD,
    RESOURCE,
    ACTION,
    OTHER
}
