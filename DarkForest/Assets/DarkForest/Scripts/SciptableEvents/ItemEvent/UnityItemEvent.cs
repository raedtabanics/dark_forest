using System;
using UnityEngine.Events;


[Serializable] 
public class UnityItemEvent : UnityEvent<ItemData> { }

