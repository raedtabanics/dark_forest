using UnityEngine;
using DapperDino.DapperTools.ScriptableEvents.Events;


[CreateAssetMenu (fileName = "New Item Event", menuName = "Game Events/Item Event")]
public class ItemEvent : BaseGameEvent<ItemData> {}