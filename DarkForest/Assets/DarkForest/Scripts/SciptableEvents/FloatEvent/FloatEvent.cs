﻿using UnityEngine;
using DapperDino.DapperTools.ScriptableEvents.Events;


[CreateAssetMenu(fileName = "New Float Event", menuName = "Game Events/Float Event")]
public class FloatEvent : BaseGameEvent<float> {}

