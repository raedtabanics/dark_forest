using System;
using UnityEngine.Events;
using UnityEngine.InputSystem;

[Serializable] 
public class InputContextUnityEvent : UnityEvent<InputAction.CallbackContext> { }
