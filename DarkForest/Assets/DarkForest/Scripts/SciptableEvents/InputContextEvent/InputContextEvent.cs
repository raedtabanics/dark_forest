using UnityEngine;
using UnityEngine.InputSystem;
using DapperDino.DapperTools.ScriptableEvents.Events;

[CreateAssetMenu (fileName = "New Input Context Event", menuName = "Game Events/Input Context Event")]
public class InputContextEvent : BaseGameEvent<InputAction.CallbackContext> {}

