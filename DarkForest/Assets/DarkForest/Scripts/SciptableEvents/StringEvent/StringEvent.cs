﻿using UnityEngine;
using DapperDino.DapperTools.ScriptableEvents.Events;


[CreateAssetMenu (fileName = "New String Event", menuName = "Game Events/String Event")]
public class StringEvent : BaseGameEvent<string> {}

