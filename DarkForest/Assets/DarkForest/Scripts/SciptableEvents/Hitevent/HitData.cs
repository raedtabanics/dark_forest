using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitData {
    
    public IHitable owner;
    public float damage;

    public HitData(IHitable owner, float damage){
        this.owner = owner;
        this.damage = damage;
    }
}
