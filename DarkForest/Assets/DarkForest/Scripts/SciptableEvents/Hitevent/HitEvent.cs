using UnityEngine;
using DapperDino.DapperTools.ScriptableEvents.Events;


[CreateAssetMenu (fileName = "New Hit Event", menuName = "Game Events/Hit Event")]
public class HitEvent : BaseGameEvent<HitData> {}