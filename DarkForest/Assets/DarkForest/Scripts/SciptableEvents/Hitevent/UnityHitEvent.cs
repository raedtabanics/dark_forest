using System;
using UnityEngine.Events;


[Serializable] 
public class UnityHitEvent : UnityEvent<HitData> { }

