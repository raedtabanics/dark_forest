using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionImpaceEventData 
{
    public Dictionary<string,int> inventoryImpactDict;
    public Dictionary<string,float> vitalImpactDict;
    public string craftAssetID;

    public ActionImpaceEventData(Dictionary<string,int> inventoryImpactDict,Dictionary<string,float> vitalImpactDict,string craftAssetID){
        this.inventoryImpactDict =inventoryImpactDict;
        this.vitalImpactDict = vitalImpactDict;
        this.craftAssetID = craftAssetID;
    }

    public ActionImpaceEventData(string craftAssetID){
        this.craftAssetID = craftAssetID;
    }

}
