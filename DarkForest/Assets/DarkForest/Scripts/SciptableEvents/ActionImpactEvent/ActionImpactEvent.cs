using UnityEngine;
using DapperDino.DapperTools.ScriptableEvents.Events;


[CreateAssetMenu (fileName = "New Action Impact Event", menuName = "Game Events/Action Impact Event")]
public class ActionImpactEvent : BaseGameEvent<ActionImpaceEventData> {}