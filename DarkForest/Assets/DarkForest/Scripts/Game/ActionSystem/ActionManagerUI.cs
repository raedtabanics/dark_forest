using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Threading.Tasks;
using DapperDino.DapperTools.ScriptableEvents.Events;
using DG.Tweening;
public class ActionManagerUI : MonoBehaviour
{

    [SerializeField]
    private CanvasGroup actionPanel,requirementPanel;
    [SerializeField]
    private Transform requirmentContainer;
    [SerializeField]
    private GameObject requirmentItemPrefab,actionbuttonPanel,JournalPanel;
    [SerializeField]
    private Text nameText,descriptionText,journalNameText,journalDescriptionText;
    public Button executeButton;

    [SerializeField]
    private ActionImpactEvent actionImpactEvent;

    [Header ("Animation Parameter")]
    [SerializeField]
    private float fadeDuration;


    private ActionItemModel itemModel;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnActionItem(ItemData itemData){
        itemModel = Session.Instance.GetActionByName(itemData.itemName);
        
        
        if(itemModel.type ==ActionType.JOURNAL){

            JournalPanel.SetActive(true);
            journalNameText.text = itemModel.actionName;
            journalDescriptionText.text = itemModel.description;
        }
        
            nameText.text = itemModel.actionName;
            descriptionText.text = itemModel.description;
            if(itemModel.requirementList.Values.Count >0){                
                SetRequirmentPanelVisibility(1);
            }
            else{
                SetRequirmentPanelVisibility(0);
            }
            actionbuttonPanel.SetActive(true);
            CheckRequirements();
            GameStatus.Instance.setGameMode(GameMode.ACTION);
            SetActionPanelVisibility(1);
              
    }

    public async void ExecuteAction(){ //
        actionImpactEvent.Raise(new ActionImpaceEventData(itemModel.requirementList,itemModel.vitalImpactList,itemModel.craftAssetId));
        SetActionPanelVisibility(0);
        await AnimProgress();
        
        GameStatus.Instance.setGameMode(GameMode.IDLE);
    }

    public void SetActionPanelVisibility(float value){
        actionPanel.DOFade(value,fadeDuration);
        if(value==0){
            
            actionbuttonPanel.SetActive(false);
            foreach(Transform child in requirmentContainer){
                Destroy(child.gameObject);
            }
            JournalPanel.SetActive(false);
        }
    }

    public void CloseActionPanel(){
        SetActionPanelVisibility(0);
        GameStatus.Instance.setGameMode(GameMode.IDLE);
    }
    [SerializeField]
    private StringEvent journalEvent;
    public void CloseJournal(){ 
        journalEvent.Raise(itemModel.name);
        CloseActionPanel();
    }


    [SerializeField]
    private GameObject progressPanel;
    [SerializeField]
    private Image progressSlider;
    [SerializeField]
    private float progressDuration;

    private async Task<bool> AnimProgress(){
        progressPanel.SetActive(true);
        await progressSlider.DOFillAmount(1,progressDuration).AsyncWaitForCompletion();
        progressPanel.SetActive(false);
        progressSlider.fillAmount=0;
        return true;


    }

    private void SetRequirmentPanelVisibility(float value){
        requirementPanel.DOFade(value,0);
    }

    private async void CheckRequirements(){
        bool isRequirementFullfilled =true;
        foreach(KeyValuePair<string, int> entry in itemModel.requirementList){
            GameObject tempObj = Instantiate(requirmentItemPrefab,requirmentContainer);
            RequirmentItemUI itemUI = tempObj.GetComponent<RequirmentItemUI>();
            itemUI.Setup(entry.Key,entry.Value.ToString());
            bool IsItemPresent = GameObject.FindObjectOfType<InventoryUI>().IsItemPresent(entry.Key,entry.Value);
            if(!IsItemPresent){
                itemUI.SetStatus(!IsItemPresent);
                isRequirementFullfilled=false;
            }
            
        }

        if(!isRequirementFullfilled)
            executeButton.interactable = false;
        else   
            executeButton.interactable = true;

    }
}
