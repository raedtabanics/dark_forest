using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RequirmentItemUI : MonoBehaviour
{
    public Text nameText,amountText;
    public Image image;
    public GameObject statusImage;

    public async  void Setup(string name,string amount){
        nameText.text = name;
        amountText.text = amount;
        
    }

    public void SetStatus(bool value){
        statusImage.SetActive(value);
    }
}
