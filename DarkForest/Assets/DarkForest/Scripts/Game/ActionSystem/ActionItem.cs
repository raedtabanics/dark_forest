using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DapperDino.DapperTools.ScriptableEvents.Events;

public class ActionItem : Item,IInteractable
{


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Interact(){
        //Fire Action event
        
    }

    public override void OnActionButton()
    {
        itemEvent.Raise(new ItemData(itemId:id, itemName:name,itemType:ItemType.ACTION,itemReference:this.gameObject));
    }
}
