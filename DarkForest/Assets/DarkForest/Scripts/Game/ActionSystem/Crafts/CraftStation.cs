using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;
public class CraftStation : MonoBehaviour
{
    [SerializeField]
    private Transform spawnPoint;
    [SerializeField]
    private GameObject spawnPS;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public async void OnActionImpact(ActionImpaceEventData impactData){
        if(impactData.craftAssetID=="")
            return;
        GameObject crafteItemPrefab = Instantiate(await Prefabs.GetAddressable(impactData.craftAssetID),spawnPoint);
        //spawnPS.Play();
        spawnPS.SetActive(true);

    }
}
