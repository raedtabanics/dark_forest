using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Resource :  Item, IInteractable
{
    public override void OnActionButton(){
        Debug.Log("test");
        itemEvent.Raise(new ItemData(itemId:id,itemName: name ,itemType:ItemType.RESOURCE,itemReference:this.gameObject));
    }
}
