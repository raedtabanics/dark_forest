using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : Item,IInteractable
{
    // Start is called before the first frame update
    public void OnActionButton(){
        itemEvent.Raise(new ItemData(itemId:id,itemName: name ,itemType:ItemType.WEAPON,itemReference:this.gameObject));
    }
    public  void OnSelection(){
        UI_Manager.Instance.SelectItem(this);
    }
    public  void OnDeselection(){
        UI_Manager.Instance.DeselectItem();
    }
}
