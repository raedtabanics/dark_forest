using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Item : MonoBehaviour, IInteractable
{
    [SerializeField]
    protected ItemEvent itemEvent;
    
    public string name;
    public string id;
  
    
    public void OnSelection(){

        UI_Manager.Instance.SelectItem(this);
    }
    public void OnDeselection(){
         UI_Manager.Instance.DeselectItem();
    }
    public virtual void OnActionButton(){
    }


}
