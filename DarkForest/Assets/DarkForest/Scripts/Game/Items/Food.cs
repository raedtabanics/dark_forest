using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DapperDino.DapperTools.ScriptableEvents.Events;
public class Food : Item, IInteractable
{
    
    public override void OnActionButton(){
        Debug.Log("test");
        itemEvent.Raise(new ItemData(itemId:id,itemName: name ,itemType:ItemType.FOOD,itemReference:this.gameObject));
    }

}
