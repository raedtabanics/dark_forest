using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SceneHelper {

    public static bool IsWithinRange(Transform player, Transform AI,  float range){
        if(Vector3.Distance(player.position,AI.position) <= range)
            return true;
        return false;

    }

    public static bool IsWithinFieldOfVision(Transform player, Transform AI,Transform camera, float range){
        Vector3 direction = AI.position - player.position;
        float angle = Vector3.Angle(direction, camera.forward);
        if(angle<=range)
            return true;
        return false;

    }

    

    public static bool IsWithinTimeOfTheDay(TimeOfDayData timeOfDay){
        if( ReferenceEquals(DayNightManager.Instance.CurrentSignificantTime,timeOfDay))
            return true;
        return false; 
    }
}
