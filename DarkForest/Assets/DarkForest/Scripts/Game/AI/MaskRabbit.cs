using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.Threading.Tasks;
public class MaskRabbit : MonoBehaviour
{

    [SerializeField]
    private Transform leftHand,rightHand;
    [SerializeField]
    private Vector3 leftHandTargetPosition,righthandTargetPosition;
    public float handDuration;
    public GameObject jumpscareRabbit;
    // Start is called before the first frame update
    void Start()
    {
       
         PlayScene();
    }

    // Update is called once per frame
    void Update()
    {
        //if(Input.GetKeyDown(KeyCode.A))
           
    }

    private async void PlayScene(){
        //Play Mask Sounds
        Component.FindObjectOfType<Feedback_UI>().MaskOn();
        await MoveInhands();
        jumpscareRabbit.SetActive(true);
        Destroy(transform.gameObject);
    }

    private async Task<bool> MoveInhands(){
        Sequence handSequence = DOTween.Sequence();
        handSequence.Append(leftHand.DOLocalMove(leftHandTargetPosition,handDuration));
        handSequence.Join(rightHand.DOLocalMove(righthandTargetPosition,handDuration));
        await handSequence.AsyncWaitForCompletion();
        return true;
    }
}
