using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Actor : MonoBehaviour
{

    private SceneBase currentScene;
    private bool isActive =false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Setup(){

    }

    // Update is called once per frame
    void Update()
    {
        if(isActive){
            currentScene.OnScenePlay();
        }
    }

    public void SwitchScene(SceneBase nextScene){
        currentScene = nextScene;
    }

    public void SetActiveStatus(bool value){
        isActive =  value;
    }
}
