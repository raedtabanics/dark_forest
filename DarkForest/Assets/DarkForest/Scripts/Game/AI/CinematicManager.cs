using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CinematicManager : Singleton<CinematicManager>
{
    [Header ("Context Parameters")]
    public float[] inBetweenScenesSpaces;
    private float timer;
    private TimeOfDayData currentSignificantTime;
    private int dayCount = 0;


    [Header ("Scene Parameters")]
    public SceneBase[] scenes;
    private int sceneIndex=0;
    private bool isInScene =false;

    //Timer 
    
    private Transform player,AI,camera;
    private Actor actorAI;

    private bool isDone=false;
    void Awake(){
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        Setup();
        PrepareScene();
    }

    private void Setup(){
        player = Component.FindObjectOfType<PlayerBattleMovement>().transform;
        camera = Component.FindObjectOfType<Camera>().transform;
        actorAI = Component.FindObjectOfType<Actor>();
        AI = actorAI.transform;
        foreach(SceneBase scene in scenes)
            scene.Setup(player,AI,camera);
    }

    // Update is called once per frame
    void Update()
    {
        if(!isDone)
            if(InBetweenSceneSpacePassed())
                if(!isInScene)
                    if(SceneRequirmentAchieved())
                        OnSceneStart();
        
    }

    private bool SceneRequirmentAchieved(){
        if(scenes[sceneIndex].OnSceneCoditionsMet())
            return true;
        return false;
    }

    private void PrepareScene(){
        Debug.Log("Playering Scene : "+ scenes[sceneIndex]);
        timer = Time.time;
        scenes[sceneIndex].OnSceneSetup();
    }

    private bool InBetweenSceneSpacePassed(){
        if(Time.time - timer >= inBetweenScenesSpaces[sceneIndex])
            return true;
        else    
            return false;
    }

    private void OnSceneStart(){
        Debug.Log("Are we here");
        //Hand AI Scene 
        isInScene = true;
        actorAI.SwitchScene(scenes[sceneIndex]);
        actorAI.SetActiveStatus(true);
    }
    public void OnSceneEnd(){
        Debug.Log("ending Scene");
        actorAI.SetActiveStatus(false);
        sceneIndex++;
        if(sceneIndex>=scenes.Length)
            isDone = true;
        sceneIndex = sceneIndex% scenes.Length;
        PrepareScene();
        isInScene= false;
    }

}
