using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName =("AI/Scenes/Stalk"))]
public class StalkScene : SceneBase
{
    public float outRange,inRange,fieldOfVision,spawnDistance;

      public override void Setup(Transform player, Transform AI,Transform camera)
    {
        base.player = player;
        base.AI = AI;
        base.camera = camera;
    }

    public override void OnSceneSetup(){
        isDone = false;
        timer =-1;
    }

    private Vector3 GetPositionRelativeToPlayer(){
        return base.player.transform.position  - base.player.transform.forward *spawnDistance;
    }
    public override bool OnSceneCoditionsMet(){
        if(SceneHelper.IsWithinFieldOfVision(player,AI,camera,fieldOfVision))
            return true;
        //Condition Not Met
        Vector3 position = GetPositionRelativeToPlayer();
        AI.transform.position = new Vector3(position.x,1.5f,position.z);
        return false;
    }

    private float timer =-1;
    private float duration=2;
    public override void OnScenePlay(){
        if(timer <0){
            timer = Time.time;
            //-Heart Stopping Sound
        }

        if(!isDone){
            if(Time.time - timer >= duration){
                Component.FindObjectOfType<UI_JumpScare>().PlayDarkScene();
                isDone=true;
                AI.transform.position = new Vector3(1000,1000,100);
                OnSceneEnd();
            }
            

            
            
            
        }  
    }

    private void Disappear(){
         AI.transform.position = new Vector3(1000,1000,100);
    }
    public override void OnSceneEnd(){
        CinematicManager.Instance.OnSceneEnd();
    }
}
