using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName =("AI/Scenes/Terror"))]
public class TerrorScene : SceneBase
{
    public float outRange,inRange,fieldOfVision,spawnDistance;

      public override void Setup(Transform player, Transform AI,Transform camera)
    {
        base.player = player;
        base.AI = AI;
        base.camera = camera;
    }

    public override void OnSceneSetup(){
        //-tension Build up Music
        isDone = false;
        timer =-1;
    }

    private Vector3 GetPositionRelativeToPlayer(){
        return base.player.transform.position  + new Vector3(base.camera.transform.forward.x,0,base.camera.transform.forward.z) *spawnDistance;
    }

    
    public override bool OnSceneCoditionsMet(){
        return true;
    }

    private float timer =-1;
    private float duration=1;
    public override void OnScenePlay(){
        if(timer <0){
            timer = Time.time;
            AI.position = GetPositionRelativeToPlayer();
            AI.LookAt(player);
            Component.FindObjectOfType<UI_JumpScare>().PlayJumpScareScene();
            //-JumpScare Sound
        }

        if(!isDone){
            if(Time.time - timer >= duration){
                isDone=true;
                AI.transform.position = new Vector3(1000,1000,100);
                OnSceneEnd();
            }
            

            
            
            
        }  
    }

    private void Disappear(){
         AI.transform.position = new Vector3(1000,1000,100);
    }
    public override void OnSceneEnd(){
        CinematicManager.Instance.OnSceneEnd();
    }
}
