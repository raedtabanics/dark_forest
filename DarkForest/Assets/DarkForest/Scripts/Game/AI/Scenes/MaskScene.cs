using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName =("AI/Scenes/Mask"))]
public class MaskScene : SceneBase
{
    public float outRange,inRange,fieldOfVision,spawnDistance;

      public override void Setup(Transform player, Transform AI,Transform camera)
    {
        base.player = player;
        base.AI = AI;
        base.camera = camera;
    }

    public override void OnSceneSetup(){
        //-tension Build up Music
        isDone = false;
    }

    private Vector3 GetPositionRelativeToPlayer(){
        return base.player.transform.position  + new Vector3(base.camera.transform.forward.x,0,base.camera.transform.forward.z) *spawnDistance;
    }

    
    public override bool OnSceneCoditionsMet(){
        return true;
    }

    public override void OnScenePlay(){
        if(!isDone){
            //-Play Mask Sounds
            Component.FindObjectOfType<UI_JumpScare>().PlayMaskScene();
            isDone=true;
            //AI.transform.position = new Vector3(1000,1000,100);
            OnSceneEnd();        
        }  
    }

    private void Disappear(){
         AI.transform.position = new Vector3(1000,1000,100);
    }
    public override void OnSceneEnd(){
        CinematicManager.Instance.OnSceneEnd();
    }
}
