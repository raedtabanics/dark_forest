using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.Threading.Tasks;
[CreateAssetMenu(menuName =("AI/Scenes/Sight"))]
public class SightScene : SceneBase
{
    public float outRange,inRange,fieldOfVision;
    public float xRange,yRange,zRange;

    public override void Setup(Transform player, Transform AI,Transform camera)
    {
        base.player = player;
        base.AI = AI;
        base.camera = camera;
    }

    public override void OnSceneSetup(){
        isDone = false;
        AI.position = GenerateRandomPosition();
    }

    private Vector3 GenerateRandomPosition(){
        return new Vector3(Random.Range(-xRange,xRange),yRange,Random.Range(-zRange,zRange));
    }
    public override bool OnSceneCoditionsMet(){
        if(SceneHelper.IsWithinRange(player, AI,outRange))
            if(SceneHelper.IsWithinFieldOfVision(player,AI,camera,fieldOfVision))
                if(SceneHelper.IsWithinRange(player,AI,inRange))
                    return true;
        return false;
    }
    public override void  OnScenePlay(){
        if(!isDone){
            //-Unsettling Music
            isDone = true;
            AI.transform.position = new Vector3(1000,1000,100);
            OnSceneEnd();
            
        }  
    }
    public override void OnSceneEnd(){
        Debug.Log("ending Scene");
        CinematicManager.Instance.OnSceneEnd();
    }

}
