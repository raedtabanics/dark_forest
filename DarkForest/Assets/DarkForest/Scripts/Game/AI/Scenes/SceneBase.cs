using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SceneBase : ScriptableObject
{
    public bool  isDone, isTimeDependent;
    public TimeOfDayData timeOfDay;
    public Transform player,AI,camera;

    
    public abstract void Setup(Transform player, Transform AI, Transform Camera);
    public abstract void OnSceneSetup();
    public abstract bool OnSceneCoditionsMet();
    public abstract void OnScenePlay();
    public abstract void OnSceneEnd();
    
}
