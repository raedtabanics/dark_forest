using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StalkyRabbit : MonoBehaviour
{

    public SoundEvent soundEvent;
    
    private Transform camera;
    public Transform player;
    public float moveSpeed,visionRange,spawnDistance,timeout;
    public Animator animator;
    private bool isActive =false;

    public GameObject maskRabbit;
    // Start is called before the first frame update
    void Start()
    {
        camera = Component.FindObjectOfType<Camera>().transform;
    }

    public void Setup(float speed, float timeout){
        this.moveSpeed = speed;
        this.timeout = timeout;
    }

    // Update is called once per frame
    void Update()
    {
        if(isActive){
            MoveTowardsPlayer();
            LookTowardsPlayer();
            Countdown();
        }
        else{

            if(CheckCondition())
                PlayScene();
            else if(!SceneHelper.IsWithinRange(player,transform,17))
                SetPostionRelativeToPlayer();
        }
        
    }

    private float timer ;
    private void Countdown(){
        if(Time.time - timer> timeout)
            this.gameObject.SetActive(false);
    }

    private void PlayScene(){
        //Play Stalk Sounds
        isActive = true;
        animator.SetBool("Stalk",true);
        timer = Time.time;
    }

    private bool CheckCondition(){
        if(SceneHelper.IsWithinFieldOfVision(player,transform,camera,visionRange) && SceneHelper.IsWithinRange(player,transform,17))
            return true;
        return false; 
    }


    
    
    private void SetPostionRelativeToPlayer(){
        Vector3 position = player.position  - player.forward *spawnDistance;
        transform.position = new Vector3(position.x,1.3f,position.z);
    }


    private void MoveTowardsPlayer(){
        transform.position += GetVectorDirection()*moveSpeed*Time.deltaTime;
    }
    private Vector3 GetVectorDirection(){
        Vector3 direction = player.position - transform.position;
        direction.y  =0; 
        return direction.normalized;
    }

    private void LookTowardsPlayer(){
        transform.LookAt(player);
    }

    private void OnTriggerEnter(Collider other){
        if(other.tag == "Player"){
            maskRabbit.SetActive(true);
            Destroy(transform.gameObject);
        }
       
    }
}
