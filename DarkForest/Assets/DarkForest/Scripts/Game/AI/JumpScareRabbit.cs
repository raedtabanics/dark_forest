using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;
using DG.Tweening;
public class JumpScareRabbit : MonoBehaviour
{
    public float timeout =8, jumpscareSpeed;    
    public Vector3 endTargetDestination,endTargetRotation;
    public Transform target;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("PlayScene",timeout);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private async void PlayScene(){
        GameStatus.Instance.setGameMode(GameMode.LOSE);
        Sequence jumpscareSequence = DOTween.Sequence();
        jumpscareSequence.Append(target.DOLocalMove(endTargetDestination,jumpscareSpeed));
        jumpscareSequence.Join(target.DOLocalRotate(endTargetRotation,jumpscareSpeed));
        await jumpscareSequence.AsyncWaitForCompletion();
    } 
}
