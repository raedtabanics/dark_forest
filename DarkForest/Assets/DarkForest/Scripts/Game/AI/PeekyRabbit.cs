using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;
using DG.Tweening;
public class PeekyRabbit : MonoBehaviour
{

    public SoundEvent soundEvent;

    [SerializeField]
    private Transform handLeft,handRight,neck,rabbit;
    private Vector3 handLeftOriginalPlacment,handRightOriginalPlacment;


    [SerializeField]
    private Vector3 startRotation,endRotation;
    public float peekInDuration,pauseDuration,peekOutDuration;
    private bool isActive =false;
    // Start is called before the first frame update
    void Start()
    {
        handLeftOriginalPlacment = handLeft.position;
        handRightOriginalPlacment = handRight.position;
    }

    // Update is called once per frame
    void Update()
    {
        if(isActive){
            Placehands();
        }
        
    }

    private void Placehands(){
        Debug.Log("Placing Hands");
        handLeft.position = handLeftOriginalPlacment;
        handRight.position = handRightOriginalPlacment;
    }

    private async void OnTriggerEnter(Collider other){
        Debug.Log("Scene Start");
        soundEvent.Play();
        //Play Creepy Animation
        isActive =true;
        await PeekAnim();
        //Component.FindObjectOfType<
        Destroy(rabbit.gameObject);
    }

    //-Animation
    private async Task<bool> PeekAnim(){

        Sequence peekSequence = DOTween.Sequence();
        peekSequence.Append(neck.DORotate(startRotation,peekInDuration).SetEase(Ease.InOutSine));
        peekSequence.Append(neck.DORotate(startRotation,pauseDuration).SetEase(Ease.InOutSine));
        peekSequence.Append(neck.DORotate(endRotation,peekOutDuration).SetEase(Ease.InOutSine));
        await peekSequence.AsyncWaitForCompletion();
        return true;
    }
}
