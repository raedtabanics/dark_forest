using UnityEngine;
using UnityEngine.UI;

public class StatUIManager : MonoBehaviour
{

    
    
    
    ///////////////////////////////////////////////////////////
    // PROPERTIES
    ///////////////////////////////////////////////////////////
    
    
    [SerializeField]
    private Image HealthMeter, StaminaMeter, HungerMeter, WarmthMeter, TirednessMeter, ChargeMeter;




    ///////////////////////////////////////////////////////////
    // METHODS
    ///////////////////////////////////////////////////////////
    

    // okay this may look a little dumb and violates D.R.Y. but it makes it so simple for Unity events to call these methods individually

    public void UpdateHealthMeter    (float value)  { HealthMeter.fillAmount    = value / 100.0f; } 
    public void UpdateStaminaMeter   (float value)  { StaminaMeter.fillAmount   = value / 100.0f; }   
    public void UpdateHungerMeter    (float value)  { HungerMeter.fillAmount    = value / 100.0f; }
    public void UpdateWarmthMeter    (float value)  { WarmthMeter.fillAmount    = value / 100.0f; } 
    public void UpdateTirednessMeter (float value)  
    { 
        //TirednessMeter.fillAmount = value / 100.0f; 
        Debug.Log (value);
    } 
    public void UpdateChargeMeter (float value)  { ChargeMeter.fillAmount = value ; } 
}