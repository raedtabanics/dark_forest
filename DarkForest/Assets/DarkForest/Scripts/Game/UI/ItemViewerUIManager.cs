using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using DG.Tweening;
using System.Threading.Tasks;
using DapperDino.DapperTools.ScriptableEvents.Events;
public class ItemViewerUIManager : Singleton<ItemViewerUIManager>

{

    [Header("References")]
    [SerializeField]
    private Text _itemName;
    [SerializeField]
    private Text _itemDescription;
    [SerializeField]
    private Image _itemImage;
    [SerializeField]
    private CanvasGroup _itemViewerPanel;
 

    [Header ("Animation Parameter")]
    [SerializeField]
    private float fadeDuration;

    
    [SerializeField]
    private ItemEvent itemEvent;

    private ItemData currentItemData;
    
    
    void Awake(){

        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        //GameStatus.Instance.OnGameModeChange +=SwitchPanel;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    
    //Delegate Called when ItemViewerEvent is raised 
    public async void SetItemOnViewer(ItemData itemData){

        currentItemData = itemData;
        await itemData.Setup();
        //Check Type of Item if weapon, food, resource or other
        switch(itemData.itemType){
            
            case ItemType.WEAPON:
                WeaponModel weaponItem = Session.Instance.GetWeaponByName(itemData.itemName);
                if(weaponItem!=null)
                    SetData(weaponItem.name,weaponItem.description,itemData.sprite);
                break;
            case ItemType.FOOD:
                FoodModel foodItem = Session.Instance.GetFoodByName(itemData.itemName);
                if(foodItem!=null)
                    SetData(foodItem.name,foodItem.description,itemData.sprite);
                break;
            case ItemType.RESOURCE:
                ResourceModel resourceItem = Session.Instance.GetResourceByName(itemData.itemName);
                if(resourceItem!=null)
                    SetData(resourceItem.name,resourceItem.description,itemData.sprite);
                break;
            default:
                Debug.Log("Sth else");
                break;


        }
        await ShowItemViewer();
        GameStatus.Instance.setGameMode(GameMode.ITEM_VIEW);
        /*
        ItemModel item  = Session.Instance._databaseModel.GetItemModelById(assetId);
        if(item!=null){

            
        }*/
    }

    private void SetData(string name, string description,Sprite sprite){
        _itemName.text = name;
        _itemDescription.text = description;
        _itemImage.sprite = sprite;
        
        
    }

    public void OnActionButton(){
        if(GameStatus.Instance.IsInItemView() && currentItemData != null){
            Debug.Log("Lets Put Item In Inventory");
            itemEvent.Raise(currentItemData);
            currentItemData.itemReference.SetActive(false);
            HideItemViewer();
        }
    }

    private async Task<bool> ShowItemViewer(){
        await _itemViewerPanel.DOFade(1,fadeDuration).AsyncWaitForCompletion();
        return true;
    }

    public void HideItemViewer(){
        Debug.Log("We are In Hide ");
        if(GameStatus.Instance.IsInItemView()){
            Debug.Log("We are In Hide?");
            _itemViewerPanel.DOFade(0,fadeDuration);
            GameStatus.Instance.setGameMode(GameMode.IDLE);
        }

        currentItemData = null;
        
    }
}
