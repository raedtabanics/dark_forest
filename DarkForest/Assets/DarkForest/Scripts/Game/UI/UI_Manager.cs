using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Threading.Tasks;
public class UI_Manager : Singleton<UI_Manager>
{

    [SerializeField]
    private Text _objectDetailText;


    [SerializeField]
    private CanvasGroup _canvasGroup;
    [SerializeField]
    private RectTransform _maskPanel;

    [SerializeField]
    private CanvasGroup _damageEffectGroup;


    private void Awake(){

        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    async void Update()
    {
        if(Input.GetKeyDown(KeyCode.B)){
            //await ShowDamage();
        }
        if(Input.GetKeyDown(KeyCode.N)){
            
        }
    }


    public void SelectItem(Item item){
        _objectDetailText.text =item.name;
        _canvasGroup.alpha = 1;
        

    }

    public void DeselectItem(){
        _objectDetailText.text="";
        _canvasGroup.alpha = 0;

    }

    public async void ShowDamage(){
        Sequence damageSequence = DOTween.Sequence();
        damageSequence.Append(_damageEffectGroup.DOFade(1,.3f).SetEase(Ease.InOutSine));
        damageSequence.Append(_damageEffectGroup.DOFade(0,.3f).SetEase(Ease.InOutSine));
        await damageSequence.AsyncWaitForCompletion();
        //return true;
      
    }

}
