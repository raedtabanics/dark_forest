using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Threading.Tasks;
using System.Linq;
public class DialogueUIManager : MonoBehaviour
{

    [SerializeField]
    private Text dialogueText;
    [SerializeField]
    private CanvasGroup dialougeBox;

    private bool isReady =true;
    // Start is called before the first frame update

    public async void OnActionButton(ItemData itemData){
        
        Debug.Log("Almost there");
        if(isReady){
            isReady =false;
            SetDialogue(itemData.itemName);
            await FadeInOut();
            isReady =true;
        }
        

    } 

    private  void SetDialogue(string name){
        dialogueText.text = Session.Instance.GetDialogueByName(name).description;
        
    }
    private async Task<bool> FadeInOut(){
        Sequence dialougeFadeInOutSequence = DOTween.Sequence();
        dialougeFadeInOutSequence.Append(dialougeBox.DOFade(1,2f));
        dialougeFadeInOutSequence.Append(dialougeBox.DOFade(0,1f));
        await dialougeFadeInOutSequence.AsyncWaitForCompletion();
        return true;
    }

}
