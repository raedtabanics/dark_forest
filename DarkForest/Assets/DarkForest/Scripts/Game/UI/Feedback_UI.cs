using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;
using DG.Tweening;
public class Feedback_UI : MonoBehaviour
{

    [SerializeField]
    private CanvasGroup maskPanel,blackoutPanel;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public async Task<bool> FadeIn(){
        await blackoutPanel.DOFade(1,1).AsyncWaitForCompletion();
        await blackoutPanel.DOFade(0,1).AsyncWaitForCompletion();
        return true;
    }

    public async Task<bool> MaskOn(){
        await blackoutPanel.DOFade(0,.5f).AsyncWaitForCompletion();
        await blackoutPanel.DOFade(1,1).AsyncWaitForCompletion();
        await blackoutPanel.DOFade(1,5).AsyncWaitForCompletion();
        await maskPanel.DOFade(1,0).AsyncWaitForCompletion();
        await blackoutPanel.DOFade(0,1).AsyncWaitForCompletion();
        return true;
    }



    
}
