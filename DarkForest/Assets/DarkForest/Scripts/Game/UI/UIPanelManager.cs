using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPanelManager : Singleton<UIPanelManager>
{

    public GameObject itemViewPanel;
    public GameObject inventoryPanel;

    void Awake(){

        Instance = this;
        
    }
    // Start is called before the first frame update
    void Start()
    {
       // GameStatus.Instance.OnGameModeChange +=SwitchPanel;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SwitchPanel(GameMode gameMode){
        switch(gameMode){
            case GameMode.IDLE:
                itemViewPanel.SetActive(false);
                break;
            case GameMode.ITEM_VIEW:
                itemViewPanel.SetActive(true);
                break;




        }

    }
}
