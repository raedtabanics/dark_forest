using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class UI_JumpScare : MonoBehaviour
{

    public CanvasGroup jumpscarePanel,stalkPanel,maskpanel;
    public RectTransform handPanel;
    public float duration;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public async void PlayJumpScareScene()
    {
        await jumpscarePanel.DOFade(0,.3f).AsyncWaitForCompletion();
        await jumpscarePanel.DOFade(1,duration).AsyncWaitForCompletion();
        jumpscarePanel.DOFade(0,0);
    }

    public float handDuration;
    public float maskBlackoutDuration,maskWakeupDuration;
     public async void PlayMaskScene()
    {
        await handPanel.DOAnchorPos(Vector2.zero,handDuration).AsyncWaitForCompletion();
        await stalkPanel.DOFade(1,.3f).AsyncWaitForCompletion();
        await handPanel.DOAnchorPos(new Vector2(-1400,0),handDuration).AsyncWaitForCompletion();
        await stalkPanel.DOFade(1,maskBlackoutDuration).AsyncWaitForCompletion();
        maskpanel.DOFade(1,0);
        await stalkPanel.DOFade(0,maskWakeupDuration).AsyncWaitForCompletion();
    }

     public async void PlayDarkScene()
    {
        await stalkPanel.DOFade(1,duration).AsyncWaitForCompletion();
        stalkPanel.DOFade(0,duration);
    }
}
