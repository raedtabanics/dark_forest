using System;
using MoreMountains.Feedbacks;
using UnityEngine;
using UnityMovementAI;

namespace DarkForest.Scripts.Game.Creatures.Boar
{
    public class BoarBehaviour : BaseCreatureBehaviour
    {
        [SerializeField] private Wander2Unit wanderBehaviour;
        [SerializeField] private SeekUnit seekBehaviour;


        private void Awake()
        {
            if (seekBehaviour.target == null)
            {
                seekBehaviour.target = GameObject.FindGameObjectWithTag("Player").transform;
            }
            
            Wander();
        }

        protected void OnTriggerEnter(Collider other)
        {
            base.OnTriggerEnter(other);

            if (other.TryGetComponent(out VitalManager manager))
            {
                Seek();
            }
        }
        
        protected void OnTriggerExit(Collider other)
        {
            base.OnTriggerExit(other);

            if (other.TryGetComponent(out VitalManager manager))
            {
                Wander();
            }
        }


        private void Wander()
        {
            seekBehaviour.enabled   = false;
            wanderBehaviour.enabled = true;
        }

        private void Seek()
        {
            wanderBehaviour.enabled = false;
            seekBehaviour.enabled   = true;
        }
    }
}