using System;
using UnityEngine;

    public class BaseCreatureBehaviour : MonoBehaviour
    {
        [SerializeField] protected AK.Wwise.Event DetectedWwiseEvent;


        protected virtual void OnTriggerEnter (Collider other)
        {
            if (other.TryGetComponent (out VitalManager manager))
            {
                Debug.Log ("Base creature detection");
            }
        }
        
        protected virtual void OnTriggerExit (Collider other)
        {
            if (other.TryGetComponent (out VitalManager manager))
            {
                Debug.Log ("Base creature detection lost");
            }
        }
    }