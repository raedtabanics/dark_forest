using System.Threading.Tasks;
using DG.Tweening;
using UnityEngine;    

public class BirdBehaviour : BaseCreatureBehaviour
{
    protected void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent (out VitalManager manager))
        {
            base.OnTriggerEnter(other);
            FleeSequence();
        }
    }

    private async Task<bool> FleeSequence()
    {
        await AnimFlyTo (RandomFleeTarget);
        Debug.Log("done fleeing");
        await AnimFlyTo (BirdManager.Instance.GetRandomRoostingSpot().position);
        return true;
    }

    private Vector3 RandomFleeTarget { get => (Vector3.up * 100.0f) + (Vector3.right * Random.Range (-100.0f, 100.0f)) + (Vector3.forward * Random.Range (-100.0f, 100.0f)); }

    private async Task<bool> AnimFlyTo (Vector3 targetPosition)
    {
        Sequence seq = DOTween.Sequence();
        
        seq.Append (this.transform.DOMove(targetPosition, 20.0f));
        seq.Join   (this.transform.DOShakeRotation (20.0f, 1.0f));

        await seq.AsyncWaitForCompletion();
        return true;
    }
}