using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class BirdManager : Singleton<BirdManager>
{
    private List<GameObject> activeBirds;
    private GameObject[] roostingSpots;


    [SerializeField] 
    private GameObject crowPrefab;


    [SerializeField] private Timer timer;
    
    public Transform GetRandomRoostingSpot()
    {
        Transform spot = roostingSpots[Mathf.FloorToInt(Random.Range(0.0f, roostingSpots.Count() + 1))].transform;
        Debug.Log(spot.name);
        return spot;
    }

    private void Awake()
    {
        Instance = this;

        roostingSpots = GameObject.FindGameObjectsWithTag("RoostingSpot ");
    }

    private void Start()
    {
        
    }

    private void OnDrawGizmos()
    {
        foreach (var spot in GameObject.FindGameObjectsWithTag ("RoostingSpot "))
        {
            Gizmos.DrawWireSphere(spot.transform.position, 0.5f);
        }
    }
}