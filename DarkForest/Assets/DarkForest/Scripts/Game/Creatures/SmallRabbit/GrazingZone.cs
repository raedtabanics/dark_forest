using System;
using UnityEngine;

[RequireComponent (typeof (SphereCollider))]
public class GrazingZone : MonoBehaviour
{

    private void Awake()
    {
        GetComponent<SphereCollider>().isTrigger = true;
    }

}