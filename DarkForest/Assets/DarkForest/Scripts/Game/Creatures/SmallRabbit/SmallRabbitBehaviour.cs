using System;
using UnityEngine;
using UnityMovementAI;


public class SmallRabbitBehaviour : BaseCreatureBehaviour
{

    [SerializeField] private EvadeUnit evadeBehaviour;
    [SerializeField] private Wander2Unit wanderBehaviour;
    [SerializeField] private SeekUnit seekBehaviour;

    [SerializeField] private float fleeTimer = 10.0f;

    private Timer timer;


    private void Awake()
    {
        SwitchToSeek();

        if (evadeBehaviour.target == null)
        {
            evadeBehaviour.target = GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<MovementAIRigidbody>();
        }
    }


    private void Update()
    {
        if (timer != null)
        {
            timer.Tick (Time.deltaTime);
        }
    }

    protected void OnTriggerEnter (Collider other)
    {
        base.OnTriggerEnter (other);
        
        if (other.TryGetComponent (out VitalManager vitalManager))
        {
            SwitchToEvade();
            Debug.Log ("Player is near bunny");
        }

        if (other.TryGetComponent (out GrazingZone zone))
        {
            if (evadeBehaviour.enabled == false)
            {
                SwitchToWander();
            }
            Debug.Log ("bunny is grazing");
        }
    }
    
    private void OnTriggerExit (Collider other)
    {
        if (other.TryGetComponent (out VitalManager vitalManager))
        {
            if (timer != null)
            {
                timer.OnTimerEnd -= SwitchToSeek;
            }
            
            timer = new Timer (fleeTimer);
            timer.OnTimerEnd += SwitchToSeek;
            Debug.Log ("Player is not near bunny");
        }
        
        if (other.TryGetComponent (out GrazingZone zone))
        {
            if (evadeBehaviour.enabled == false)
            {
                SwitchToSeek();
            }
        }
    }

    
    // yeah yeah, this shit is indeed lazy

    private void SwitchToEvade()
    {
        seekBehaviour.enabled   = false;
        wanderBehaviour.enabled = false;
        evadeBehaviour.enabled  = true;
    }


    private void SwitchToWander()
    {
        evadeBehaviour.enabled  = false;
        seekBehaviour.enabled   = false;
        wanderBehaviour.enabled = true;

        Debug.Log("Wandering");
    }


    private void SwitchToSeek()
    {
        evadeBehaviour.enabled  = false;
        wanderBehaviour.enabled = false;
        seekBehaviour.enabled   = true;

        SeekClosestGrazingZone();
    }


    private void SeekClosestGrazingZone()
    {
        float closestDistance = Mathf.Infinity;
        Transform closestZone = null;

        foreach (var zone in FindObjectsOfType<GrazingZone>())
        {
            float dist = (zone.transform.position - this.transform.position).sqrMagnitude;
            if (dist < closestDistance)
            {
                closestDistance = dist;
                closestZone = zone.transform;
            }
        }

        seekBehaviour.target = closestZone;
    }
}