using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
public class Inventory 
{
    public event Action<ItemModel> OnItemListChanged;
    public List<InventoryItem> itemList;

    
    public Inventory(){

        itemList = new List<InventoryItem>();
    }
    public InventoryItem GetItemById(string assetId){
        return itemList.FirstOrDefault((x)=> x.assetId == assetId);
    }

    public void AddItem(string assetId){
        ItemModel itemModel = Session.Instance._databaseModel.GetItemModelById(assetId);
        InventoryItem item = new InventoryItem{assetId = itemModel.assetId,name = itemModel.name};
        itemList.Add(item);
        OnItemListChanged?.Invoke(itemModel);
    }

    public void removeItem(){
        //To Do
    }

    public List<InventoryItem> GetInventory(){
        return itemList;
    }


}
