using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;
using DG.Tweening;
using UnityEngine.UI;
using System.Linq;
using System;
public class InventoryUI : Singleton<InventoryUI>
{
    [SerializeField]
    private Transform _inventoryContainer;
    [SerializeField]
    private GameObject _inventroyItemTemplate;

    private Inventory _inventory;

    [SerializeField]
    private float fadeDuration;
    
    [SerializeField]
    private CanvasGroup _inventoryPanel;

    [SerializeField]
    private CanvasGroup dataContainer;


    [SerializeField]
    private Text desctiptionText;
    [SerializeField]
    private Image image;
    [SerializeField]
    private GameObject weaponActionPanel,foodActionPanel;


    private List<GameObject> inventoryObjectList = new List<GameObject>();
    private ItemData currentSelectedItem;

    [SerializeField]
    private ItemEvent weaponEquipEvent,itemDropEvent;
    [SerializeField]
    private ActionImpactEvent actionImpactEvent;
    void Awake(){
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        //Setup();

    }


    public void Setup(Inventory inventory){
        //_inventoryContainer = transform.Find("InventoryContainer");
        //_inventroyItemTemplate = _inventoryContainer.Find("InventroyItemTemplate");
        //_inventroyItemTemplate.gameObject.SetActive(false);
        _inventory = inventory;
        _inventory.OnItemListChanged +=OnItemListChanged;
        Debug.Log(_inventroyItemTemplate.name);
    }

    // Update is called once per frame
    async void Update()
    {
        if(Input.GetKeyDown(KeyCode.I)){
            if(GameStatus.Instance.IsInInventory()){
                await HideInventory();
                SetCurserStatus(false);
                Reset();
                GameStatus.Instance.setGameMode(GameMode.IDLE);
            }
            else if(GameStatus.Instance.IsInIDLE()){

                await ShowInventory();
                SetCurserStatus(true);
                GameStatus.Instance.setGameMode(GameMode.INVENTORY);
            }
            
            

        }
    }

    private async Task<bool> HideInventory(){
        
        await _inventoryPanel.DOFade(0,fadeDuration).AsyncWaitForCompletion();
        SetDataContainerVisibility(0);
        return true;
    }
    private async Task<bool> ShowInventory(){
        await _inventoryPanel.DOFade(1,fadeDuration).AsyncWaitForCompletion();
        return true;
    }

    private void SetCurserStatus(bool value){
        Cursor.visible = value;
        if(value)
            Cursor.lockState = CursorLockMode.None;
        else
            Cursor.lockState = CursorLockMode.Locked;


    }

    private async Task<bool> SetDataContainerVisibility(float value){
        await dataContainer.DOFade(value,.5f).AsyncWaitForCompletion();
        return true;
    }

    private void OnItemListChanged(ItemModel item){
        ItemUI tempItem = Instantiate(_inventroyItemTemplate,_inventoryContainer).GetComponent<ItemUI>();
        tempItem.nameText.text = item.name;
    }

    public void PutItem(ItemData itemData){
        string itemName = itemData.itemName;
        ItemUI item = IsItemPresent(itemName);
        if(item!=null){
            item.UpdateCount(1);
            Debug.Log("We Area sadasdasdsad");

        }
        else{
            GameObject tempItemButton = Instantiate(_inventroyItemTemplate,_inventoryContainer);
            ItemUI itemUI = tempItemButton.GetComponent<ItemUI>();
            itemUI.Setup(itemData);
        }
        
    }

    public async void OnSelectItem(ItemData item){
        currentSelectedItem = item;
        if(item.itemType == ItemType.WEAPON){

            weaponActionPanel.SetActive(true);
            foodActionPanel.SetActive(false);
            desctiptionText.text = Session.Instance.GetWeaponByName(item.itemName).description;
        }
        else if(item.itemType == ItemType.FOOD){
            weaponActionPanel.SetActive(false);
            foodActionPanel.SetActive(true);
            desctiptionText.text = Session.Instance.GetFoodByName(item.itemName).description;
        }
        else if(item.itemType == ItemType.RESOURCE){
            weaponActionPanel.SetActive(false);
            foodActionPanel.SetActive(false);
            desctiptionText.text = Session.Instance.GetResourceByName(item.itemName).description;
        }
        image.sprite = item.sprite;
        await SetDataContainerVisibility(1);

    }


    public bool IsItemPresent(string name, int amount){
        ItemUI[] itemUIList = _inventoryContainer.GetComponentsInChildren<ItemUI>();
        Debug.Log("Looking for "+name +" in list of "+itemUIList.Length);
        if(itemUIList.Any((x)=>x.itemName == name) ){
            ItemUI requiredItem =itemUIList.FirstOrDefault((x)=>x.itemName == name);
            if( requiredItem.GetCount()>=amount)
                return true;
            else
                return false;
        }
        else
            return false;
    
    }

    public ItemUI IsItemPresent(string name){
        ItemUI[] itemUIList = _inventoryContainer.GetComponentsInChildren<ItemUI>();
        if(itemUIList.Any((x)=>x.itemName == name)){
            Debug.Log("Item Found");
            return itemUIList.FirstOrDefault((x)=> x.itemName == name);
        }
        Debug.Log("Item Not Found");
        return null;
        
    }

    public void RemoveItemsFromInventory(ActionImpaceEventData impactData){
        ItemUI[] itemUIList = _inventoryContainer.GetComponentsInChildren<ItemUI>();
        
        foreach(KeyValuePair<string,int> entry in impactData.inventoryImpactDict){
            ItemUI toBeDropped =itemUIList.First((x)=>x.itemName == entry.Key);
            if(toBeDropped && toBeDropped.GetCount()-entry.Value >=1){
                toBeDropped.UpdateCount(-entry.Value);  
            }
            else if(toBeDropped){
                Destroy(toBeDropped.gameObject);
            }
        }
    }

    private void Reset(){
         desctiptionText.text ="";

    }

    public void EquipWeapon(){
        ItemUI[] itemUIList = _inventoryContainer.GetComponentsInChildren<ItemUI>();
        ItemUI toBeEquipped = itemUIList.FirstOrDefault((x)=>x.itemName == currentSelectedItem.itemName);
        toBeEquipped.SetEquipStatus();
        Component.FindObjectOfType<PlayerBattleMovement>().EquipWeapon();
    }

    public void UseItem(){
        //ActionItemModel itemModel = Session.Instance.GetActionByName(currentSelectedItem.itemName);
        FoodModel itemModel = Session.Instance.GetFoodByName(currentSelectedItem.itemName);
        actionImpactEvent.Raise(new ActionImpaceEventData(new Dictionary<string, int>(),itemModel.vitalImpactList,""));
        ConsumeItem();
    }

    private void ConsumeItem(){
        ItemUI[] itemUIList = _inventoryContainer.GetComponentsInChildren<ItemUI>();
        ItemUI toBeDropped = itemUIList.FirstOrDefault((x)=>x.itemName == currentSelectedItem.itemName);
        if(IsItemPresent(currentSelectedItem.itemName) && toBeDropped.GetCount()>1){

            toBeDropped.UpdateCount(-1);    
        }
        else{
            
            Destroy(toBeDropped.gameObject);

        }
        
        
    }

    public async void DropItem(){
        /*
        currentSelectedItem.itemReference.SetActive(true);
        currentSelectedItem.itemReference.transform.position = Component.FindObjectOfType<PlayerBattleMovement>().transform.position ;*/
        itemDropEvent.Raise(currentSelectedItem);
        await SetDataContainerVisibility(0);
        DropItemFromInventory();
    }

    private void DropItemFromInventory(){
        ItemUI[] itemUIList = _inventoryContainer.GetComponentsInChildren<ItemUI>();
        ItemUI toBeDropped = itemUIList.FirstOrDefault((x)=>x.itemName == currentSelectedItem.itemName);
        if(IsItemPresent(currentSelectedItem.itemName) && toBeDropped.GetCount()>1){

            toBeDropped.UpdateCount(-1);    
        }
        else{
            
            Destroy(toBeDropped.gameObject);

        }
        
    }
}
