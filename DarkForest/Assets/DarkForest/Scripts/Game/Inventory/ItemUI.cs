using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ItemUI : MonoBehaviour
{
    public string itemName;
    public string assetId;
    public ItemType itemType;
    public Text nameText,countText;
    private int itemCount;


    public Image image,equipStatusImage;

    private ItemData ItemData;

    public void Setup(ItemData itemData){
        itemName = itemData.itemName;
        assetId = itemData.itemId;
        itemType =itemData.itemType;
        this.ItemData = itemData;
        nameText.text = itemName;
        itemCount = 1;
        countText.text = itemCount +"";
        image.sprite = itemData.sprite;
    }

    public int GetCount(){
        return itemCount;
    }
    

    public void UpdateCount(int value){
        itemCount +=value;
        countText.text = itemCount +"";

    }

    public void SetEquipStatus(){
        equipStatusImage.enabled=true;
    }
    public void DebugTest(){

        Component.FindObjectOfType<InventoryUI>().OnSelectItem(ItemData);
    }

    /*
    public void OnWeaponEquiped(ItemData itemData){
        if(itemType == ItemType.WEAPON){
            if(itemName.Equals(itemData.itemName)){
                Debug.Log("We are in UI Item");
            }
        }
        
    }

    
    public void OnItemDropped(ItemData itemData){
        if(itemType == ItemType.WEAPON){
            if(itemName.Equals(itemData.itemName)){
                Debug.Log("We are in UI Item");
            }
        }
        
    }
    */

    
}
