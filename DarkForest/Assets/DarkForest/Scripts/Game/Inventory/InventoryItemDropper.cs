using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryItemDropper : MonoBehaviour
{
    public float yBias =1f;
    public void DropItem(ItemData itemData){
        Debug.Log("We are here at least");
        itemData.itemReference.SetActive(true);
        itemData.itemReference.transform.position = transform.position;
    }

    private Vector3 GetPositionOnGround(){
        Ray ray = new Ray(transform.position,Vector3.back);
        RaycastHit hit;
        if(Physics.Raycast(ray, out hit, 10)){
            return hit.point + Vector3.down*yBias;
        }
        return transform.position;
    }
}
