using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class PlayerEventManager : Singleton<PlayerEventManager>
{

    public event Action OnAttackButton;
    public event Action OnAttackCharge;
    void Awake(){
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void FireOnAttackButton(){
        OnAttackButton?.Invoke();
    }
     public void FireOnAttackCharge(){
        OnAttackCharge?.Invoke();
    }
}
