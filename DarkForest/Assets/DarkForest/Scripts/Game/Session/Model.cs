using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


public class DatabaseModel{

    public List<WeaponModel> weaponList = new List<WeaponModel>();
    public List<ResourceModel> resourceList = new List<ResourceModel>();
    public List<FoodModel> foodList = new List<FoodModel>();
    public List<DialougeItemModel> dialougeItemList = new List<DialougeItemModel>();
    public List<ActionItemModel> actionItemList = new List<ActionItemModel>();

    public ItemModel GetItemModelById(string id){
        return null;
    }
}

public class ActionItemModel{
 
    public string name;
    public string assetId;

    public ActionType type;
    public string actionName;
    public Dictionary<string,int> requirementList;
    public Dictionary<string,float> vitalImpactList;
    public string craftAssetId;
    public string description;


}

public enum ActionType{
        REST,
        FIRE,
        JOURNAL,
        CRAFT
}
public class WeaponModel{
    public string name;
    public string assetId;
    public string description;
}
public class ResourceModel{
    public string name;
    public string assetId;
    public string description;
}

public class FoodModel{
    public string name;
    public string assetId;
    public string description;
    public Dictionary<string,float> vitalImpactList;
}

public class DialougeItemModel{
    public string name;
    public string assetId;
    public string description;

}

public class ItemModel{
    public string name;
    public string assetId;
    public string description;

}



