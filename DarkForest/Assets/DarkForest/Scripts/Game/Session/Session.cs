using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Linq;
public class Session : Singleton<Session>
{
    [Header("Temporary References")]
    [SerializeField]
    private TextAsset _configJSON;

    public  DatabaseModel _databaseModel;
    public List<WeaponModel> weaponList;
    public List<ResourceModel> resourceList;
    public List<FoodModel> foodList;
    public List<DialougeItemModel> dialogueItemList;
    public List<ActionItemModel> actionItemList;
    void Awake(){

        Instance = this;

        Cursor.visible   = false;
        Cursor.lockState = CursorLockMode.Locked;
    }


 
    // Start is called before the first frame update
    async void Start()
    {
        _databaseModel = await LoadConfig();
        weaponList= _databaseModel.weaponList;
        resourceList = _databaseModel.resourceList;
        foodList = _databaseModel.foodList;
        dialogueItemList = _databaseModel.dialougeItemList;
        actionItemList =_databaseModel.actionItemList;
    }

    private async Task<DatabaseModel> LoadConfig(){
        return JsonConvert.DeserializeObject<DatabaseModel>(_configJSON.text);
    }

    public WeaponModel GetWeaponByName(string name){
        return weaponList.FirstOrDefault((x)=>x.name.ToLower() == name.ToLower());
    }
    public ResourceModel GetResourceByName(string name){
        return resourceList.FirstOrDefault((x)=>x.name.ToLower() == name.ToLower());
    }
    public FoodModel GetFoodByName(string name){
        return foodList.FirstOrDefault((x)=>x.name.ToLower() == name.ToLower());
    }
    public DialougeItemModel GetDialogueByName(string name){
        return dialogueItemList.FirstOrDefault((x)=>x.name.ToLower() == name.ToLower());
    }

    public ActionItemModel GetActionByName(string name){
        return actionItemList.FirstOrDefault((x)=>x.name.ToLower() == name.ToLower());
    }


}
