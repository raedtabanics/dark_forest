using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameScore : MonoBehaviour
{
    public GameObject portal;
    public List<string> journalsDiscovered = new List<string>();
    private const int JOURNAL_MAX_COUNT=5;

    public GameObject stalkRabbit;

    public AnimationCurve speedPerjournal,timeoutPerJournal;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AddJournal(string journalName){
        string name = journalName.ToLower();
        if(IsJournalDiscoveredAlready(name))
            return;
        Debug.Log("adding Journal "+name);
        journalsDiscovered.Add(name);
        CheckJournalProgress();
        SpawnStalker();
    }

    private bool IsJournalDiscoveredAlready(string journalName){
        if(journalsDiscovered.Contains(journalName))
            return true;
        return false;
    }

    private void CheckJournalProgress(){
        if(journalsDiscovered.Count>= JOURNAL_MAX_COUNT)
            portal.SetActive(true);
    }

    private void SpawnStalker(){
        int journalCount = journalsDiscovered.Count;
        
        GameObject Stalky = Instantiate(stalkRabbit);
        Stalky.GetComponent<StalkyRabbit>().Setup(speedPerjournal.Evaluate(journalCount),timeoutPerJournal.Evaluate(journalCount));
        Stalky.SetActive(true);
    }

}
