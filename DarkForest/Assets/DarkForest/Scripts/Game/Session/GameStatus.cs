using System.Collections;
using System.Collections.Generic;
using DapperDino.DapperTools.ScriptableEvents.Events;
using UnityEngine;
using System;
public class GameStatus : Singleton<GameStatus>
{

    public event Action<GameMode> OnGameModeChange;
    [SerializeField]
    private VoidEvent onEquipButton;
    void Awake(){

        Instance = this;

    }

    public GameMode gameMode;
    // Start is called before the first frame update
    void Start()
    {
        setGameMode(GameMode.IDLE);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SwitchBattleMode(){

       
        if(gameMode == GameMode.IDLE)
            gameMode = GameMode.BATTLE;
        else if(gameMode == GameMode.BATTLE)
            gameMode = GameMode.IDLE;

        Debug.Log(gameMode);
    }

    public void setGameMode(GameMode gameMode){

        this.gameMode = gameMode;
        OnGameModeChange?.Invoke(gameMode);
        SetMouseStatus();
    }

    private void SetMouseStatus(){
        if(gameMode == GameMode.IDLE || gameMode == GameMode.BATTLE){
            Cursor.visible =false;
            Cursor.lockState = CursorLockMode.Locked;
        }
        else{
            Cursor.visible =true;
            Cursor.lockState = CursorLockMode.None;

        }
    }

    public GameMode getGameMode(){

        return gameMode;
    }

    #region Query GameMode
    public bool IsInIDLE(){
        if(gameMode == GameMode.IDLE)
            return true;
        return false;
    }
    public bool IsInBattle(){
        if(gameMode == GameMode.BATTLE)
            return true;
        return false;
    }
    public bool IsInItemView(){
        if(gameMode == GameMode.ITEM_VIEW)
            return true;
        return false;
    }
    public bool IsInInventory(){
        if(gameMode == GameMode.INVENTORY)
            return true;
        return false;
    }

    public bool IsInAction(){
        if(gameMode == GameMode.ACTION)
            return true;
        return false;
    }


    #endregion
}


public enum GameMode{
    IDLE,
    ITEM_VIEW,
    BATTLE,
    INVENTORY,
    ACTION,
    PAUSE,
    CUTSCENE,
    WIN,
    LOSE

}