using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class FootstepBehaviour : SerializedMonoBehaviour
{
    
    public enum FootstepMaterial
    {
        Grass, TallGrass, Wood, RiverShore
    }


    private FootstepMaterial currentFootstepMaterial = FootstepMaterial.Grass;


    ///////////////////////////////////////////////////////////
    // PROPERTIES
    ///////////////////////////////////////////////////////////
    
    
    [SerializeField]
    private CMF.Controller controller;


    [SerializeField]
    private CharacterActions characterActions;


    [SerializeField]
    private Animator animator;


    [SerializeField]
    private CMF.Mover mover;


    [SerializeField]
    private AK.Wwise.Event FootstepEvent;

    
    [SerializeField] 
    private AK.Wwise.Switch WalkSwitch, RunSwitch;

    
    [SerializeField]
    private Dictionary<FootstepMaterial, AK.Wwise.Switch> SwitchBank;


    [SerializeField]
    private LayerMask layerMask;


    [SerializeField]
    private Transform feetTransform = null;



    //Whether footsteps will be based on the currently playing animation or calculated based on walked distance (see further below);
    public bool useAnimationBasedFootsteps = false;

    //Velocity threshold for landing sound effect;
    //Sound effect will only be played if downward velocity exceeds this threshold;
    public float landVelocityThreshold = 5f;

    //Footsteps will be played every time the traveled distance reaches this value (if 'useAnimationBasedFootsteps' is set to 'true');
    public float footstepDistance = 5.5f;
    float currentFootstepDistance = 0.0f;

    private float currentFootStepValue = 0.0f;








    //Setup;
    void Start() 
    {
        //Connecting events to controller events;
        // controller.OnLand += OnLand;
        // controller.OnJump += OnJump;

        // if (animator == null)
        // {
        //     useAnimationBasedFootsteps = false;
        // }

        WalkSwitch.SetValue (this.gameObject);

        if (feetTransform == null)
        {
            feetTransform = this.transform;
        }
    }


    public void ShouldRun (bool shouldRun)
    {
        if (shouldRun)
        {
            RunSwitch.SetValue (this.gameObject);
            return;
        }

        WalkSwitch.SetValue (this.gameObject);
    }
    




    public void PlayFootstepSound()
    {
        RaycastHit hit;

        if (Physics.Raycast (this.transform.position, -this.transform.up, out hit, 1.0f, layerMask))
        {
            switch (LayerMask.LayerToName (hit.transform.gameObject.layer))
            {
                case "Wood":
                    currentFootstepMaterial = FootstepMaterial.Wood;
                    break;


                case "Grass":
                default:
                    currentFootstepMaterial = FootstepMaterial.Grass;
                    break;                
            }
        }

        SwitchBank[currentFootstepMaterial].SetValue (gameObject);
        FootstepEvent.Post (gameObject);
    }



    private void OnLand (Vector3 _v)
    {
        //Only trigger sound if downward velocity exceeds threshold;
        if (CMF.VectorMath.GetDotProduct (_v, this.transform.up) > -landVelocityThreshold)
            return;

        //Play land audio clip;
        //FootstepEvent.
    }

    private void OnJump (Vector3 _v)
    {
        //Play jump audio clip;
        //audioSource.PlayOneShot(jumpClip, audioClipVolume);
    }


    private void SwitchFootstepMaterial (FootstepMaterial newMaterial)
    {
        
    }

}