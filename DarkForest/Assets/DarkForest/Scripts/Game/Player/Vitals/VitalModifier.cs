using System.Collections.Generic;
using System;
using UnityEngine;


[CreateAssetMenu(fileName = "New Vital Modifier", menuName = "Vital Modifier")]
public class VitalModifier : ScriptableObject
{


    ///////////////////////////////////////////////////////////
    // PROPERTIES and GETTERS
    ///////////////////////////////////////////////////////////


    [SerializeField]
    public float InitialValueToAdd, ValueAddedPerSecond, Duration;


    public float GetInitialValueToAdd()   { return InitialValueToAdd; }
    public float GetValueAddedPerSecond() { return ValueAddedPerSecond; }

    
    
    
    ///////////////////////////////////////////////////////////
    // TIMER
    ///////////////////////////////////////////////////////////
    

    private Timer _timer;


    private void HandleTimerEnd() 
    {
        OnTimerEnd?.Invoke (this);
    }


    public delegate void TimerEndHandler (VitalModifier modifier);


    // VitalBase uses this event to hook into this modifiers timer event so VitalBase can automatically remove 
    // this modifier with event, instead of wasting time checking if this modifier's timer has expired.
    public event TimerEndHandler OnTimerEnd;


    
    
    ///////////////////////////////////////////////////////////
    // METHODS
    ///////////////////////////////////////////////////////////
    
    
    public void Step()
    {
        if (_timer != null)
        {
            _timer.Tick (Time.deltaTime);
            return;
        }

        // if timer is null, go ahead and call TimerEnd to remove this modifier from any VitalBase
        //HandleTimerEnd();
    }


    
    public void Init()
    {
        if (Duration > 0.0f)
        {
            _timer = new Timer (Duration);
            _timer.OnTimerEnd += HandleTimerEnd;
            return;
        }

        _timer = null;
    }



    public void Delete()
    {
        if (_timer != null)
        {
            _timer.OnTimerEnd -= HandleTimerEnd;
        }
    }


}
