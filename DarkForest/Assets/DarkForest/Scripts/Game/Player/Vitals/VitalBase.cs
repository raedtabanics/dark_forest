using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using DapperDino.DapperTools.ScriptableEvents.Events;
using UnityEngine.Events;


[CreateAssetMenu(fileName = "New Vital", menuName = "Vital")]
public class VitalBase : ScriptableObject
{


    ///////////////////////////////////////////////////////////
    // PROPERTIES
    ///////////////////////////////////////////////////////////



    // [SerializeField]
    // protected String vitalName;




    [SerializeField]
    protected float MinValue = 0, MaxValue = 100;



    // how many points do you gain or lose per second; 
    // for stats that lose value over time like tiredness, this value should be negative.
    // stats that regenerate, like stamina, this value should be positive. 
    // static stats like health should be zero.
    [SerializeField]
    protected float ChangePerSecond = 0;




    [SerializeField]
    protected float StartingValue = 100;


    protected float _currentValue;


    public float CurrentValue { get => _currentValue; }


    public void Init()
    {
        _currentValue = StartingValue;
        OnValueChanged.Raise (_currentValue);
    }

    

    [SerializeField]
    private FloatEvent OnValueChanged;
    
    

    [SerializeField]
    private VoidEvent OnVitalReachesZero;



    
    ///////////////////////////////////////////////////////////
    // METHODS
    ///////////////////////////////////////////////////////////



    public void AddAmount (float amount)
    {
        _currentValue = Mathf.Clamp (_currentValue + amount, MinValue, MaxValue);
        OnValueChanged.Raise (_currentValue / 100.0f);
    }
    


    private bool ShouldCopyModifiers = false;


    public void AddModifier (VitalModifier modifier)
    {
        // add delegate in case this modifier uses a timer, and if it doesn't, this won't have any side effects (hopefully lol)
        modifier.Init();
        modifier.OnTimerEnd += RemoveModifier;

        AddAmount (modifier.GetInitialValueToAdd());

        _modifiers.Add (modifier);

        ShouldCopyModifiers = true;
        
        Debug.Log (string.Format ("Adding {0} modifier to {1}", modifier.name, this.name));
    }



    public void RemoveModifier (VitalModifier modifier)
    {
        modifier.OnTimerEnd -= RemoveModifier;
        modifier.Delete();
        _modifiers.Remove (modifier);
        ShouldCopyModifiers = true;

        Debug.Log (string.Format ("Removing {0} modifier to {1}", modifier.name, this.name));
    }



    
    ///////////////////////////////////////////////////////////
    // UPDATE
    ///////////////////////////////////////////////////////////
    

    // Used to apply status effects to this vital
    private HashSet<VitalModifier> _modifiers = new HashSet<VitalModifier>();


    private HashSet<VitalModifier> _stableModifiers = new HashSet<VitalModifier>();
    private HashSet<VitalModifier> StableModifiers
    {
        get 
        {
            if (ShouldCopyModifiers)
            {
                _stableModifiers = new HashSet<VitalModifier> (_modifiers);
                ShouldCopyModifiers = false;
            }

            return _stableModifiers;
        }
    }



    /*
    *   This method gets called in the VitalManager as part of its update loop
    *   so that we have better control on how these get optimized. Probably overkill but oh well.
    */
    
    public void Step()
    {

        if (ChangePerSecond != 0.0f)
        {
            _currentValue += ChangePerSecond * Time.deltaTime;
        }


  
        foreach (VitalModifier modifier in StableModifiers)
        {
            modifier.Step();
            _currentValue += modifier.GetValueAddedPerSecond() * Time.deltaTime;
        }
        


        _currentValue = Mathf.Clamp (_currentValue, MinValue, MaxValue);
        

        if (_currentValue <= MinValue + float.Epsilon)
        {
            OnVitalReachesZero.Raise();
            //Debug.Log (string.Format ("{0} vital is empty!", this.name));
            return;
        }


        OnValueChanged.Raise (_currentValue / 100.0f);
    }


}