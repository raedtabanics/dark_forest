using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.Events;

public class VitalManager : SerializedMonoBehaviour
{



    // struct Vitals : IEnumerable
    // {
    //     public VitalBase Health, Stamina, Hunger, Warmth, Tiredness;


    //     public IEnumerator GetEnumerator()
    //     {
    //         throw new NotImplementedException();
    //     }
    // }


    // [SerializeField]
    // private Vitals _vitals;
    
    
    ///////////////////////////////////////////////////////////
    // PROPERTIES
    ///////////////////////////////////////////////////////////
    


    [SerializeField]
    private float hungerStarveTimeMultipler = 1.0f, tirednessTimeMultipler = 1.0f, warmthTimeOfDayMultipler = 1.0f;


    [SerializeField] 
    private VitalModifier warmthTimeOfDayModifier;
    
    private float timeOfDayWarmthX = 0.0f;

    [SerializeField] 
    private AnimationCurve TimeOfDayWarmthCurve;
    
    

    [SerializeField]
    public Dictionary<String, VitalBase> _vitals = new  Dictionary<String, VitalBase>();





    // violating D.R.Y. is a small crime to commit in order to get very simple, type safe, loosely-coupled events in the editor

    public void AddModifierToHealth     (VitalModifier modifier)  { _vitals["Health"].AddModifier    (modifier); }
    public void AddModifierToStamina    (VitalModifier modifier)  { _vitals["Stamina"].AddModifier   (modifier); }
    public void AddModifierToHunger     (VitalModifier modifier)  { _vitals["Health"].AddModifier    (modifier); }
    public void AddModifierToWarmth     (VitalModifier modifier)  { _vitals["Warmth"].AddModifier    (modifier); }
    public void AddModifierToTiredness  (VitalModifier modifier)  { _vitals["Tiredness"].AddModifier (modifier); }


    public void RemoveModifierToHealth     (VitalModifier modifier)  { _vitals["Health"].RemoveModifier    (modifier); }
    public void RemoveModifierToStamina    (VitalModifier modifier)  { _vitals["Stamina"].RemoveModifier   (modifier); }
    public void RemoveModifierToHunger     (VitalModifier modifier)  { _vitals["Health"].RemoveModifier    (modifier); }
    public void RemoveModifierToWarmth     (VitalModifier modifier)  { _vitals["Warmth"].RemoveModifier    (modifier); }
    public void RemoveModifierToTiredness  (VitalModifier modifier)  { _vitals["Tiredness"].RemoveModifier (modifier); }



    public void AddAmountToHealth     (float amount)  { _vitals["Health"].AddAmount (amount); }
    public void AddAmountToStamina    (float amount)  { _vitals["Stamina"].AddAmount (amount); }  
    public void AddAmountToHunger     (float amount)  { _vitals["Hunger"].AddAmount (amount); }
    public void AddAmountToWarmth     (float amount)  { _vitals["Warmth"].AddAmount (amount); }
    public void AddAmountToTiredness  (float amount)  { _vitals["Tiredness"].AddAmount (amount); }


    public void AddTimeToHunger    (float dt) { _vitals["Hunger"]   .AddAmount (dt * hungerStarveTimeMultipler * 0.001f); }
    public void AddTimeToTiredness (float dt) { _vitals["Tiredness"].AddAmount (dt * tirednessTimeMultipler * 0.001f); }


    public void SetWarmthTimeOfDay(string timespan)
    {
        TimeSpan time = TimeSpan.Parse(timespan);
        timeOfDayWarmthX = time.Hours + (time.Minutes / 60.0f);
        warmthTimeOfDayModifier.ValueAddedPerSecond = TimeOfDayWarmthCurve.Evaluate (timeOfDayWarmthX) * warmthTimeOfDayMultipler;
        //Debug.Log (warmthTimeOfDayModifier.ValueAddedPerSecond);
    }




    ///////////////////////////////////////////////////////////
    // UNITY METHODS
    ///////////////////////////////////////////////////////////
    

    private void OnEnable()
    {
        _vitals["Warmth"].AddModifier (warmthTimeOfDayModifier);
        foreach (var vital in _vitals)
        {
            vital.Value.Init();
        }

        StartCoroutine (UpdateRoutine());
    }


    private void OnDisable()
    {
        _vitals["Warmth"].AddModifier (warmthTimeOfDayModifier);
        StopAllCoroutines();
    }


    

    //////////////////    
    // steps one vital per frame; seems more reasonable than every vital on every frame.

    private IEnumerator UpdateRoutine()
    {
        while (true)
        {
            foreach (var vital in _vitals)
            {
                vital.Value.Step();
                yield return null;
            }
        }
    }


    public void OnActionImpact(ActionImpaceEventData impactData){
        foreach(KeyValuePair<string,float> impact in impactData.vitalImpactDict){
            if(_vitals.ContainsKey(impact.Key))
                _vitals[impact.Key].AddAmount(impact.Value);
        }
    }



    private void OnTriggerEnter (Collider other)
    {
        //Debug.Log("Something collided with warmth");

        if (other.gameObject.TryGetComponent (out WarmthSource source))
        {
            Debug.Log(string.Format("Hello warmth source with {0} value", source.WarmthModifier));
            _vitals["Warmth"].AddModifier (source.WarmthModifier);
        }
    }



    private void OnTriggerExit (Collider other) 
    {
        
        if (other.gameObject.TryGetComponent (out WarmthSource source))
        {
            //Debug.Log (string.Format ("Goodbye warmth source with {0} value", source.WarmthModifier));
            _vitals["Warmth"].RemoveModifier (source.WarmthModifier);
        }
    }


}