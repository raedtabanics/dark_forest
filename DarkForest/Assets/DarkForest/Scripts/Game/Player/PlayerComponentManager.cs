using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerComponentManager : Singleton<PlayerComponentManager>
{

    private SelectionManager _selectionManager;
    private Inventory _inventory;
    void Awake(){
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        Setup();
    }
    private void Setup(){
        _selectionManager = FindObjectOfType<SelectionManager>();
        _inventory = new Inventory();
        InventoryUI.Instance.Setup(_inventory);
        Debug.Log(_selectionManager.name);
    }
    // Update is called once per frame
    void Update()
    {
        
    }

    public SelectionManager GetSelectionManager(){

        return _selectionManager;
    }

    public void AddInventoryItem(string assetId){
        _inventory.AddItem(assetId);
    }

}
