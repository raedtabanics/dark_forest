using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using System.Threading.Tasks;
public class RayProvider : MonoBehaviour
{

    private Mouse _mouse;
    private Camera _camera;
    // Start is called before the first frame update
     void Start()
    {
      
    }

    public async Task Setup(){
       
        TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();
        _mouse = Mouse.current;
        _camera = GameObject.FindObjectOfType<Camera>() as Camera;
        tcs.SetResult(true);
        await tcs.Task;
         

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public Ray CreateRay(){
        Ray ray = _camera.ScreenPointToRay(_mouse.position.ReadValue());
        return ray;

    }
}
