using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionManager : MonoBehaviour
{
    /*
    private Selector _selector;
    private RayProvider _rayProvider;
    private Transform _selection,_previousSelection;

    private bool isReady =true;
    // Start is called before the first frame update
    async void Start()
    {
        
        Setup();
        await _rayProvider.Setup();
        
    }

    private void Setup(){

        _selector = GetComponent<Selector>();
        _rayProvider = GetComponent<RayProvider>();
    }

    // Update is called once per frame
    void Update()
    {
        
        
                _selection = _selector.GetSelection(_rayProvider.CreateRay());
                if(_selection!=null){
                //Debug.Log(_selection.name);

                    _previousSelection = _selection;
                    _previousSelection.GetComponent<IInteractable>().OnSelection();
                
                }else{
                    
                    if(_previousSelection != null){

                        _previousSelection.GetComponent<IInteractable>().OnDeselection();
                        _previousSelection = null;
                    }
                    
                    UI_Manager.Instance.DeselectItem();
                }

        
        
    }


    public void SelectItem(){

        if(_previousSelection != null){
            
            //ItemViewerUIManager.Instance.SetItemOnViewer(_previousSelection.GetComponent<Item>().GetAssetId());
            GameStatus.Instance.setGameMode(GameMode.ITEM_VIEW);
        }
    }

    public void TakeItem(){

         if(_previousSelection != null){

            PlayerComponentManager.Instance.AddInventoryItem(_previousSelection.GetComponent<Item>().GetAssetId());
            DestroyItem();
            GameStatus.Instance.setGameMode(GameMode.IDLE);
        }

    }

    private void DestroyItem(){
        //_previousSelection.GetComponent<IInteractable>().OnDeselection();
        Destroy(_previousSelection.gameObject);
        //_previousSelection=null;
    }

    */
}
