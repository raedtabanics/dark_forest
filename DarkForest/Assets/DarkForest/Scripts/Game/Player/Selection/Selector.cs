using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selector : MonoBehaviour
{

    [Header ("Ray Paramaters")]
    [SerializeField]
    private float _rayRadius;
    [SerializeField]
    private float _rayDistance;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public Transform GetSelection(Ray ray){
        RaycastHit hit;
        //Physics.SphereCast(ray,_rayRadius,out hit, _rayDistance)
        if(Physics.Raycast(ray,out hit, _rayDistance)){

            if(hit.transform.GetComponent<IInteractable>() != null){

                return hit.transform;
            }

        }
        
        return null;

    }
}
