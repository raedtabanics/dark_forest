using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
public class PlayerInputManager : MonoBehaviour
{   
    /*
    private Perspective _perspective;
    private PlayerActionMap _playerActionMap;
    private PlayerMovement _playerMovement;
    private PlayerBattleMovement _playerbattleMovement;
    void OnEnable(){

        _perspective = Component.FindObjectOfType<Perspective>();
        _playerMovement = GetComponent<PlayerMovement>();
        _playerbattleMovement= GetComponent<PlayerBattleMovement>();
        _playerActionMap = new PlayerActionMap();
        _playerActionMap.Player.Enable();
        SetupButtons();
    }

    private void SetupButtons(){
        _playerActionMap.Player.ActionButton.started +=OnActionButton;
        _playerActionMap.Player.ActionButton.canceled +=OnActionButton;
        _playerActionMap.Player.EquipButton.performed += OnEquipButton;
        _playerActionMap.Player.DashButton.started += OnDashButton;
        _playerActionMap.Player.DashButton.canceled +=OnDashButton; 
        _playerActionMap.Player.JumpButton.started +=OnJumpButton;
        _playerActionMap.Player.JumpButton.canceled +=OnJumpButton;

    }
        

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        OnMouseDelta(_playerActionMap.Player.MouseMovement.ReadValue<Vector2>());
        OnMovement(_playerActionMap.Player.Movement.ReadValue<Vector2>());
    }

    public void OnJumpButton(InputAction.CallbackContext context){
        if(context.phase ==InputActionPhase.Started){
            
            if(GameStatus.Instance.getGameMode() == GameMode.IDLE || GameStatus.Instance.getGameMode() == GameMode.BATTLE){

                _playerMovement.OnJumpCommand();
            }
        }
        else if(context.phase == InputActionPhase.Canceled){
            
            if(GameStatus.Instance.getGameMode() == GameMode.IDLE || GameStatus.Instance.getGameMode() == GameMode.BATTLE){


            }
        }

    }

    public void OnDashButton(InputAction.CallbackContext context){

        if(context.phase ==InputActionPhase.Started){
            
            if(GameStatus.Instance.getGameMode() == GameMode.IDLE || GameStatus.Instance.getGameMode() == GameMode.BATTLE){

                if(context.phase ==InputActionPhase.Started){
            
                    _playerMovement.UpdateSpeed(dash:true);
                }
                
            }
        }
        else if(context.phase == InputActionPhase.Canceled){
            
            if(GameStatus.Instance.getGameMode() == GameMode.IDLE || GameStatus.Instance.getGameMode() == GameMode.BATTLE){
                if(context.phase == InputActionPhase.Canceled){
                    
                    _playerMovement.UpdateSpeed(dash:false);
                }


            }
        }
       


    }
    public void OnActionButton(InputAction.CallbackContext context){
        

        if(context.phase ==InputActionPhase.Started){
            if(GameStatus.Instance.getGameMode() == GameMode.IDLE){
                PlayerComponentManager.Instance.GetSelectionManager().SelectItem();


            }else if(GameStatus.Instance.getGameMode() == GameMode.ITEM_VIEW){
                PlayerComponentManager.Instance.GetSelectionManager().TakeItem();


            }else if(GameStatus.Instance.getGameMode() == GameMode.BATTLE){
                //_playerbattleMovement.AttackCommand();
                PlayerEventManager.Instance.FireOnAttackCharge();
                //if(context.phase == InputActionPhase.Performed)
                

            }
        }
        else if(context.phase == InputActionPhase.Canceled){

            if(GameStatus.Instance.getGameMode() == GameMode.BATTLE){

            PlayerEventManager.Instance.FireOnAttackButton();
            }
        }
        

    }

     public void OnActionButtonUp(InputAction.CallbackContext context){
       
        
    }


    public void OnMouseDelta(Vector2 context){

        if(GameStatus.Instance.getGameMode() == GameMode.IDLE || GameStatus.Instance.getGameMode() == GameMode.BATTLE ){
            Vector2 deltaVector = context;
            _perspective.UpdateMouseLook(deltaVector);

        }
        


    }

    public void OnMovement(Vector2 context){
        if(GameStatus.Instance.getGameMode() == GameMode.IDLE || GameStatus.Instance.getGameMode() == GameMode.BATTLE ){
            Vector2 deltaVector = context;
            _playerMovement.UpdatePosition(deltaVector.normalized);
        
        }


    }

    public void OnEquipButton(InputAction.CallbackContext context){

        
        if(GameStatus.Instance.getGameMode() == GameMode.IDLE){
            GameStatus.Instance.setGameMode(GameMode.BATTLE);
            //PlayerComponentManager.Instance.GetSelectionManager().SelectItem();

        }else if(GameStatus.Instance.getGameMode() == GameMode.ITEM_VIEW){

            GameStatus.Instance.setGameMode(GameMode.IDLE);
        }
        else if(GameStatus.Instance.getGameMode() == GameMode.BATTLE){

            GameStatus.Instance.setGameMode(GameMode.IDLE);
        }


        


    }
    */
}
