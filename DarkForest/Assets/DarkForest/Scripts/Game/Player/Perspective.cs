using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
public class Perspective : MonoBehaviour
{

    private Transform playerTransform,cameraTransform;

    [Header ("Perspective Parameters")]
    [Range(0,.5f)]
    public float _mouseSmoothTime =.1f;
    [SerializeField]
    private bool lockCurser = true;
    [SerializeField]
    private float mouseSensitivity =.5f;
    [SerializeField]
    [Range(0,120)]
    private float _verticalLookRange;
    private float _cameraPitch =0;

    private Vector2 _currentMouseDelta =Vector2.zero, _currentMouseDeltaVelocity=Vector2.zero;
    
  

    // Start is called before the first frame update
    void Start()
    {
        Setup();
    }

    private void Setup(){
        playerTransform = Transform.FindObjectOfType<PlayerInputManager>().transform;
        cameraTransform = Transform.FindObjectOfType<Camera>().transform;
        if(lockCurser){
            Cursor.lockState = CursorLockMode.Locked;
        }
        Cursor.visible = false;


    }

    // Update is called once per frame
    void Update()
    {
        //Vector2 delta = Mouse.current.delta.ProcessValue;
    }

    public void UpdateMouseLook(Vector2 deltaVector){
       

        _currentMouseDelta = Vector2.SmoothDamp(_currentMouseDelta,deltaVector,ref _currentMouseDeltaVelocity,_mouseSmoothTime);

        _cameraPitch -=_currentMouseDelta.y;
        _cameraPitch = Mathf.Clamp(_cameraPitch,-_verticalLookRange,_verticalLookRange);
        cameraTransform.localEulerAngles = Vector3.right * _cameraPitch *mouseSensitivity;
        playerTransform.Rotate( Vector3.up* _currentMouseDelta.x *mouseSensitivity);

        
    }
}
