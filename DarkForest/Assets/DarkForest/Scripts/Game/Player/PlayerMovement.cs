using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    private Rigidbody _rigidBody;
    
    [Header("Movement Parameters")]
    
    //public float _walkSpeed;
    //public float _dashSpeed;
    public AnimationCurve _blendMovementSpeed;
    public float _movementSmoothTime =.3f;
    private float _movementSpeed;
    private float _movementFactor;
    private int _movementFactorDirection;
    private bool _isJumping = false, _isJumpPressed=false;


    [Header("Jump Parameter")]
    public AnimationCurve _blendJumpCurve;
    public float jumpPower,gravityFactor;
    private float _jumpFactor,_jumpVelocity,_yVelocity;
    
    private Vector2 _currentDirection = Vector2.zero,_currentDirectionVelocity=Vector2.zero;
    private CharacterController _characterController;
    // Start is called before the first frame update

    void Awake(){
        _rigidBody = GetComponent<Rigidbody>();
        _characterController = GetComponent<CharacterController>();
        UpdateSpeed(dash:false);
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(_movementSpeed);
        UpdateMovementSpeed();
        HandleGravity();
        HandleJump();


        
    }

    private void UpdateMovementSpeed(){
        _movementFactor +=_movementFactorDirection * Time.deltaTime;
        _movementFactor = Mathf.Clamp(_movementFactor,0,1); 
        _movementSpeed = _blendMovementSpeed.Evaluate(_movementFactor);

    }
    public void UpdatePosition(Vector2 deltaVector){
        
        
        //Vector3 velocity = (transform.forward * deltaVector.y + transform.right*deltaVector.x)*_movementSpeed;
        _currentDirection = Vector2.SmoothDamp(_currentDirection,deltaVector,ref _currentDirectionVelocity,_movementSmoothTime);
        Vector3 velocity = (transform.forward * _currentDirection.y +transform.right*_currentDirection.x)*_movementSpeed + Vector3.up*_yVelocity;
        _characterController.Move(velocity* Time.deltaTime  );

        

    }

    public void UpdateSpeed(bool dash){
        if(dash)
            _movementFactorDirection=1;
        else
            _movementFactorDirection=-1;


    }

    private void HandleGravity(){

        if(_characterController.isGrounded){
            _yVelocity=-.05f;
            _isJumping = false;
            //Debug.Log("Grounded");
        }
        _yVelocity += gravityFactor *Time.deltaTime;
        //_characterController.Move(Vector3.up*_yVelocity);
    }

    public void OnJumpCommand(){
        if(_isJumping)
            return;
        //_jumpFactor=0;
        Debug.Log("Jumping");
        _isJumpPressed = true;
        

    }

    private void HandleJump(){

        if(_characterController.isGrounded && !_isJumping && _isJumpPressed){
            _isJumping=true;
            _isJumpPressed=false;
            _yVelocity =jumpPower;
        }
    }



    
   
}
