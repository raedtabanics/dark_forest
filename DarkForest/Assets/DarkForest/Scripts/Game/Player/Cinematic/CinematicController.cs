using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
[RequireComponent (typeof(Camera))]
public class CinematicController : MonoBehaviour
{

    private Camera camera;

    [Header ("View Paramaters")]
    [SerializeField]
    private float maxFieldOfView,minFieldOfView;
    [SerializeField]
    private float smoothDampTime;
    private float targetFieldOfView;
    private float targetVelocity;
    private bool isFocused=false;

    [Header ("Animation Paramter")]
    [SerializeField]
    private float shakeDuration,shakeStrength;
    // Start is called before the first frame update
    void Start()
    {
        camera = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateFieldOfView();
    }

    private void UpdateFieldOfView(){

        if(isFocused)
            targetFieldOfView = minFieldOfView;
        else
            targetFieldOfView = maxFieldOfView;

        camera.fieldOfView = Mathf.SmoothDamp(camera.fieldOfView,targetFieldOfView,ref targetVelocity,smoothDampTime);
    }

    public void SetFocusOn(){
        isFocused = true;
    }
    public void SetFocusOff(){
        isFocused = false;
    }

    public void ShakeCamera(){
        camera.DOShakePosition(shakeDuration,shakeStrength);
    }
}
