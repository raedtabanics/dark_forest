using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.Threading.Tasks;
using DapperDino.DapperTools.ScriptableEvents.Events;
public class LongRangeWeapon : WeaponBase
{
    [Header ("Animation Properties")]
    public Animator animator;
    
    [Header ("Projectile Param")]
    public Camera main;
    public Vector3 pointA, pointB;
    protected GameObject projectile;

    [Header ("Charge Param")]
    public float chargeSpeed,recoilSpeed;
    public float chargeValue;
    protected bool isAttacking = false, isCharging = false ;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        main  = Component.FindObjectOfType<Camera>(); //Need it for Projectile
    }

    public override void OnDrawWeaponUp(){
        animator.SetBool("Draw",true);
    }
    public override void OnDrawWeaponDown(){
        animator.SetBool("Draw",false);
    }

    public override void OnWeaponAttack(){
        
        isCharging = false;
    }


    public override void Charge(){
        isCharging = true;
        this.chargeValue += chargeSpeed * Time.deltaTime;
        
    }


    public override float GetChargeValue(){
        return 0;
    }

    public virtual void Update(){
        
        if(!isCharging)
            this.chargeValue -= recoilSpeed * Time.deltaTime;
        chargeValue = Mathf.Clamp(chargeValue,0,1);
        //float newFOV = 60 - 20*chargeValue;
        //projectilePoint.localPosition = Vector3.Lerp(pointA,pointB,chargeValue);
    }

}
