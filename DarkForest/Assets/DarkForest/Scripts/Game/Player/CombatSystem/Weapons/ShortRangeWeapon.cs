using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShortRangeWeapon : WeaponBase
{

    [Header ("Weapon Properties")]
    public float damageAmplitude;
    public AnimationCurve damagePercentage;
    public float chargeSpeed;
    public float chargeValue;


    //replace with collider?
    private bool isAttacking,isCharging;


    [Header ("Animation Properties")]
    public Animator animator;


    public GameObject heavyAttackLineTrail,lineTrail;
    
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        heavyAttackLineTrail = transform.Find("HeavyAttackLineTrail").gameObject;
        lineTrail =  transform.Find("LineTrail").gameObject;
    }

    public override void OnDrawWeaponUp(){
        animator.SetBool("Draw",true);
    }
    public override void OnDrawWeaponDown(){
        animator.SetBool("Draw",false);
    }

    public override void OnWeaponAttack(){
        if(chargeValue < 1)
            LightAttack();
        else
            HeavyAttack();
        isAttacking = true;
        isCharging = false;
        chargeValue =0;
    }

    private void LightAttack(){
        lineTrail.SetActive(true);
        animator.SetTrigger("Attack");
    }
    private void HeavyAttack(){
        animator.SetTrigger("HeavyAttack");
        heavyAttackLineTrail.SetActive(true);
    }

    public void OnAnimationEnd(){
        Debug.Log("We Are HERE YEAYYYYYY!");
        isAttacking=false;
        lineTrail.SetActive(false);
        heavyAttackLineTrail.SetActive(false);
    }

    public  bool IsAttacking(){
        return isAttacking;
    }

    public override void Charge(){
        isCharging = true;
    }
    public override float GetChargeValue(){
        return chargeValue;
    }

    
    // Update is called once per frame
    void Update()
    {
        if(isCharging)
            chargeValue += chargeSpeed*Time.deltaTime;
        chargeValue = Mathf.Clamp(chargeValue,0,1);
    }

    private void OnTriggerEnter(Collider other){
        if(other.CompareTag("Portal"))
            other.GetComponent<IHitable>().GetHit(1);
    }

}
