using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class BowLineRenderer : MonoBehaviour
{
    private LineRenderer line;
    
    [SerializeField]
    private Transform[] bowPoints;

    private void OnEnable()
    {
        Application.onBeforeRender += UpdatePositions;
    }
 
    private void OnDisable()
    {
        Application.onBeforeRender -= UpdatePositions;
    }
    // Start is called before the first frame update
    void Start()
    {
        line = GetComponent<LineRenderer>();
        line.positionCount = bowPoints.Length;
    }

    // Update is called once per frame
    void Update()
    {
        //UpdatePositions();
    }

    private  void UpdatePositions(){
        for( int i=0;i<bowPoints.Length;i++){
            line.SetPosition(i,bowPoints[i].position);
        }
    }
}
