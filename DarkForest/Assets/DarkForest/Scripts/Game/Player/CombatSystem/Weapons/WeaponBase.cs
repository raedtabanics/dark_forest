using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.Threading.Tasks;

public abstract class WeaponBase : MonoBehaviour
{
    

    public abstract void OnDrawWeaponUp();
    public abstract void OnDrawWeaponDown();

    public abstract void OnWeaponAttack();
    
    public abstract void Charge();
    public abstract float GetChargeValue();

}
