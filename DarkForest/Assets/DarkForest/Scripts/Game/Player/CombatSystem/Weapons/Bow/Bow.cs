using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.Threading.Tasks;
public class Bow : LongRangeWeapon
{
    [Header ("Projectile Param")]
    [SerializeField]
    private GameObject projectilePrefab;
    public  Transform projectilePoint;
    public float projectileDrawSpeed;
    public float shootForce;
    public AnimationCurve shootPerCharge;


 

    public override void OnWeaponAttack(){
        Debug.Log("Lets Discharge the projectile");
        projectile.GetComponent<Projectile>().ShootCall(GetForceValue());
        isCharging = false;
    }

    public async override void Charge()
    {
        //base.Charge();
        
        projectile = Instantiate(projectilePrefab,projectilePoint);
        projectile.transform.localPosition = Vector3.one;
        await PlaceProjectile();
        isCharging = true;
        this.chargeValue += chargeSpeed * Time.deltaTime;
    }

       private async Task<bool> PlaceProjectile(){
        await projectile.transform.DOLocalMove(Vector3.zero,projectileDrawSpeed).AsyncWaitForCompletion();
        return true;
    }

    public override void Update(){
        
        if(!isCharging)
            this.chargeValue -= recoilSpeed * Time.deltaTime;
        else
            this.chargeValue += chargeSpeed * Time.deltaTime;
        chargeValue = Mathf.Clamp(chargeValue,0,1);
        //float newFOV = 60 - 20*chargeValue;
        
        //Debug.Log("Are we jjjjj");
        projectilePoint.localPosition = Vector3.Lerp(pointA,pointB,chargeValue);
    }

    private float GetForceValue(){
        return shootForce*chargeValue;
    }
}
