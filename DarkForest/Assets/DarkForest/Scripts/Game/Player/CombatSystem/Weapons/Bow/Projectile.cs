using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    // Start is called before the first frame update
    private float gravityForce =-8.9f;
    public Rigidbody rb;
    public Transform tip;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShootCall(float v0){
        transform.parent = null;
        Camera main = Component.FindObjectOfType<Camera>();
        Ray ray =main.ViewportPointToRay(new Vector3(.5f,.5f,0));
        rb.isKinematic=false;
        //rb.AddForce(ray.direction * v0,ForceMode.Impulse);
        rb.AddForceAtPosition(ray.direction * v0,tip.position,ForceMode.Impulse );
        //StartCoroutine(Shoot(v0,angle));
    }

    private IEnumerator Shoot(float v0, float angle){
        float t =0;
        while(t<10){
            float x = v0*t*Mathf.Cos(angle);
            float y = v0*t*Mathf.Sin(angle) - (.5f)* gravityForce * Mathf.Pow(t,2);

            transform.localPosition =  new Vector3(transform.position.x,y,x);
            t+=Time.deltaTime;
            yield return null;
        }
    }

    public void OnCollisionEnter(Collision other){
        Debug.Log(other.collider.name);
        rb.velocity =Vector3.zero;
        rb.isKinematic = true;
    }
}
