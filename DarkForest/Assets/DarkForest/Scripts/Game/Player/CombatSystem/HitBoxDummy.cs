using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitBoxDummy : MonoBehaviour,IHitable
{
    [SerializeField]
    private HitEvent hitEvent;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GetHit(float damage){

        HitData hitData = new HitData( owner: this , damage: damage);
        hitEvent.Raise(hitData);
    }

    
}
