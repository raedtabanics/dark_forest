using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.Threading.Tasks;
public class weaponItemWorld : MonoBehaviour
{

    
    [Header ("Weapon Properties")]
    public WeaponType weaponType;
    public float damageAmplitude;
    public AnimationCurve damagePercentage;
    public float chargeSpeed;
    public float chargeValue;


    [Header ("Animation Properties")]
    public Vector3 weaponDownPosition,weaponDownRotation,weaponUpPosition,weaponUpRotation;
    public float drawWeaponDuration,attackReach,attackDuration;



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Charge(){
        chargeValue +=chargeSpeed * Time.deltaTime;
    }


    public async Task<bool> AnimWeaponUp(Transform weaponHolder){
        Sequence weaponUpSequence = DOTween.Sequence();
        weaponUpSequence.Append(weaponHolder.DOLocalRotate( weaponUpRotation,drawWeaponDuration));
        weaponUpSequence.Join(weaponHolder.DOLocalMove( weaponUpPosition,drawWeaponDuration));
        //await weaponHolder.DOLocalRotate( new Vector3(0,0,0),.5f).AsyncWaitForCompletion();
        await weaponUpSequence.AsyncWaitForCompletion();
        return true;
    }

    
    public async Task<bool> AnimAttack(Transform weaponHolder){
        
        
        Sequence weaponAttackSequence = DOTween.Sequence();
        weaponAttackSequence.Append(weaponHolder.DOPunchPosition(Vector3.forward * attackReach ,attackDuration,2));
        weaponAttackSequence.Join(weaponHolder.DOPunchRotation( new Vector3(130,-1.5f,150),attackDuration));
        //await weaponHolder.DOLocalRotate( new Vector3(0,0,0),.5f).AsyncWaitForCompletion();
        await weaponAttackSequence.AsyncWaitForCompletion();
        return true;
    }

    public async Task<bool> AnimWeaponDown(Transform weaponHolder){
        //await weaponHolder.DOLocalRotate( new Vector3(0,0,150),.5f).AsyncWaitForCompletion();
        Sequence weaponDownSequence = DOTween.Sequence();
        weaponDownSequence.Append(weaponHolder.DOLocalRotate( weaponDownPosition,drawWeaponDuration));
        weaponDownSequence.Join(weaponHolder.DOLocalMove( weaponDownPosition,drawWeaponDuration));
        //await weaponHolder.DOLocalRotate( new Vector3(0,0,0),.5f).AsyncWaitForCompletion();
        await weaponDownSequence.AsyncWaitForCompletion();
        return true;
    }

    public float GetDamage(){
        return damageAmplitude * damagePercentage.Evaluate(chargeValue);

    }
}

public enum WeaponType {
        SHORTRANGE,
        LONGRANGE
    }
