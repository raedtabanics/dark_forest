using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using DapperDino.DapperTools.ScriptableEvents.Events;
using System.Threading.Tasks;
using DG.Tweening;
public class PlayerBattleMovement : MonoBehaviour
{
    public Transform handTarget;
    private bool _isReady = false , isCharging = false;
    //private bool _isAttacking =false , isCharging = false;

    [SerializeField]
    private Transform weaponHolder;
   
    [SerializeField]
    private InputContextEvent onActionButtonHold;

    [SerializeField]
    private WeaponBase tempWeapon;
    public WeaponBase currentWeapon;
    private float damage;
    // Start is called before the first frame update
    void Start()
    {

    }


    // Update is called once per frame
    private void Update()
    {
        if(GameStatus.Instance.getGameMode() == GameMode.BATTLE){
            if(!_isReady){   
                _isReady = true;
                currentWeapon = tempWeapon;
                currentWeapon.OnDrawWeaponUp();
            }
        }
        else{
            if(_isReady){
                _isReady = false;
                currentWeapon.OnDrawWeaponDown();
                //currentWeapon = null;
            }
        }
        if(isCharging){
            Debug.Log("is Charging");
            Component.FindObjectOfType<StatUIManager>().UpdateChargeMeter(currentWeapon.GetChargeValue());
        }
        
    }


    public void OnActionButtonHold(InputAction.CallbackContext context){
        if(GameStatus.Instance.getGameMode() == GameMode.BATTLE){
            if(context.phase == InputActionPhase.Canceled){
                if(_isReady){
                    Debug.Log("We are attacking");
                    isCharging = false;
                    currentWeapon.OnWeaponAttack();
                    Component.FindObjectOfType<StatUIManager>().UpdateChargeMeter(0);
                }
            }
            else if(context.phase == InputActionPhase.Performed){
                Debug.Log("Holding");
                currentWeapon.Charge();
                isCharging =true;
            }    
        }
    }
    public void OnWeaponEquiped(){
        Debug.Log("are we ever hear?");
        if(GameStatus.Instance.IsInBattle()){
            GameStatus.Instance.setGameMode(GameMode.IDLE);
            
        }            
        else if (currentWeapon!=null){
            Debug.Log("RRRRRRRRRRR");
            GameStatus.Instance.setGameMode(GameMode.BATTLE);
        }
            
    }
    public void EquipWeapon(){
        //Debug.Log("Please tell me we are not here!");
        currentWeapon = tempWeapon;
    }

    
}
