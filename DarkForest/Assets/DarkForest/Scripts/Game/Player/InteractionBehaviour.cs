using UnityEngine;

public class InteractionBehaviour : MonoBehaviour
{

    [SerializeField]
    private Transform cameraTransform;


    [SerializeField]
    private float Distance = 3.0f, Radius = 0.5f;



    public IInteractable previousInteractable {get ; private set;}
    public IInteractable CurrentInteractable { get; private set; }



    private void Update()
    {

        
        if(GameStatus.Instance.IsInIDLE()  || GameStatus.Instance.IsInBattle() ){
            RaycastHit hit;
            if (Physics.SphereCast (cameraTransform.position, Radius, cameraTransform.forward, out hit, Distance))
            {
                CurrentInteractable = hit.transform.GetComponent<IInteractable>();
                if (CurrentInteractable != null)
                {
                    CurrentInteractable.OnSelection();
                    previousInteractable = CurrentInteractable;
                }else{
                    if(previousInteractable != null){
                        previousInteractable.OnDeselection();
                    }
                    previousInteractable= null;                 
                }
               
            }else{
                if(previousInteractable!= null){
                        previousInteractable.OnDeselection();
                    }
                    previousInteractable= null;
            }
        }else{
            if(previousInteractable!= null){
                    previousInteractable.OnDeselection();
                }
            previousInteractable=null;
        }

        
        
    }

    public void OnActionButton(){
        Debug.Log("About to Fire");
        if(GameStatus.Instance.IsInIDLE() && previousInteractable != null){
            Debug.Log("Firring");
            previousInteractable.OnActionButton();
            
        }

    }

    public void OnPutToInventory(){
        Debug.Log("Putting to Inventot");
    }

}