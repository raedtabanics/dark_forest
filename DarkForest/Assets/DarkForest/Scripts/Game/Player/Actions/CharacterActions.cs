﻿using System.Collections;
using System.Collections.Generic;
using CMF;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using DapperDino.DapperTools.ScriptableEvents.Events;
using UnityEngine;


//This character movement input class is an example of how to get input from a keyboard to control the character;
public class CharacterActions : CharacterInput
{

	
	
	
	///////////////////////////////////////////////////////////
	// INHERITED PROPERTIES
	///////////////////////////////////////////////////////////
	
	

	public string horizontalInputAxis = "Horizontal";
	public string verticalInputAxis = "Vertical";
	public KeyCode jumpKey = KeyCode.Space;

	//If this is enabled, Unity's internal input smoothing is bypassed;
	public bool useRawInput = true;



	
	
	
	///////////////////////////////////////////////////////////
	// OUR EVENTS
	///////////////////////////////////////////////////////////
	
	

	private PlayerActionMap _playerActionMap;


	[SerializeField]
	private VHS.FirstPersonController controller;



	[SerializeField]
	private VoidEvent OnJumpButtonEvent, OnActionButtonEvent, OnDashStartEvent, OnDashEndEvent, OnEquipDropEvent;
	
	
	[SerializeField]
	private InputContextEvent OnActonButtonHold;


	private void Start()
	{
		_playerActionMap = new PlayerActionMap();
		_playerActionMap.Player.Enable();


		//_playerActionMap.Player.JumpButton.started         += OnJumpButton;
		_playerActionMap.Player.ActionButton.started       += OnActionButton;
		// _playerActionMap.Player.DashButton.started         += OnDashStart; // moved to FirstPersonController
		// _playerActionMap.Player.DashButton.canceled        += OnDashEnd; 
		_playerActionMap.Player.EquipButton.performed      += OnEquipButton;
		_playerActionMap.Player.ActionButtonHold.performed += OnActionButtonHold;
		_playerActionMap.Player.ActionButtonHold.canceled  += OnActionButtonHold;
	}


	public void OnJumpButton       (InputAction.CallbackContext context) { OnJumpButtonEvent.Raise(); }
	public void OnActionButton     (InputAction.CallbackContext context) { OnActionButtonEvent.Raise(); }
	public void OnDashStart        (InputAction.CallbackContext context) { OnDashStartEvent.Raise(); }
	public void OnDashEnd          (InputAction.CallbackContext context) { OnDashEndEvent.Raise(); }

	public void OnEquipButton      (InputAction.CallbackContext context) { OnEquipDropEvent.Raise(); }
	public void OnActionButtonHold (InputAction.CallbackContext context) { OnActonButtonHold.Raise(context); }
	
	
	///////////////////////////////////////////////////////////
	// METHODS INHERITED FROM ASSET
	///////////////////////////////////////////////////////////
	

	public float GetMovementInputMagnitude()
	{
		return GetHorizontalMovementInput() + GetVerticalMovementInput();
	}

	

	public override float GetHorizontalMovementInput()
	{
		if(useRawInput)
			return Input.GetAxisRaw(horizontalInputAxis);
		else
			return Input.GetAxis(horizontalInputAxis);
	}

	public override float GetVerticalMovementInput()
	{
		if(useRawInput)
			return Input.GetAxisRaw(verticalInputAxis);
		else
			return Input.GetAxis(verticalInputAxis);
	}

	public override bool IsJumpKeyPressed()
	{
		return Input.GetKey(jumpKey);
	}
}
