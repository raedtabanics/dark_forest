﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (SphereCollider))]
public class WarmthSource : MonoBehaviour
{

    public VitalModifier WarmthModifier;



    private SphereCollider m_Collider;

    private void Awake()
    {
        m_Collider = GetComponent<SphereCollider>();
        m_Collider.isTrigger = true;
    }
}
