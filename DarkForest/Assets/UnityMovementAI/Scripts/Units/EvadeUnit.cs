﻿using UnityEngine;

namespace UnityMovementAI
{
    public class EvadeUnit : MonoBehaviour
    {
        public MovementAIRigidbody target;

        SteeringBasics steeringBasics;
        Evade evade;

        void Start()
        {
            steeringBasics = GetComponent<SteeringBasics>();
            evade = GetComponent<Evade>();

            if (target == null)
            {
                target = GameObject.FindGameObjectWithTag("Vitals").GetComponent<MovementAIRigidbody>();
            }
        }

        void FixedUpdate()
        {
            Vector3 accel = evade.GetSteering(target);

            steeringBasics.Steer(accel);
            steeringBasics.LookWhereYoureGoing();
        }
    }
}